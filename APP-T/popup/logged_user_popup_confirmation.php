<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="../LOGOS/gc-favicon.png">
  <title>Consultora | APP</title>
  <link rel='shortcut icon' type='image/png' href='' />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.css">
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../assets/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="../assets/plugins/morris/morris.css">
  <link rel="stylesheet" href="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="../assets/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="../assets/plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="../assets/fn/fn_working_popup.css">
</head>



<!--Scripts Work -->
<script src="../assets/js/work-header/googleApisJqueryMins.js"></script>
<script src="../assets/js/work-header/jquery-1.9.1.min.js.js"></script>
<script src="../assets/js/work-header/jquery-2.1.1.min.js.js"></script>
<script type="text/javascript" src="../assets/js/functions/jquery.mask.js"></script>
<script type="text/javascript" src="../assets/js/functions/funciones.js"></script>
<!-- Cookies -->
<script src="../assets/js/functions/cookies.js"></script>

<!-- FN Javascript -->
<script src="../assets/js/config.js"></script>
<script src="../assets/js/APIhelpers.js"></script>
<script src="../assets/js/helpers.js"></script>
<script src="../assets/js/login.js"></script>
<script src="../assets/js/logout.js"></script>
<script src="../assets/js/signup.js"></script>
<script src="../assets/js/invoices.js"></script>
<script src="../assets/js/outvoices.js"></script>
<script src="../assets/js/appReadUserForId.js"></script>
<script src="../assets/js/appReadInvoices.js"></script>
<script src="../assets/js/appReadOutvoices.js"></script>
<script src="../assets/js/appReadTotalBilledInvoices.js"></script>
<script src="../assets/js/appReadTotalBilledInvoicesBiAnnual.js"></script>
<script src="../assets/js/appReadTotalBilledInvoicesAnnual.js"></script>
<script src="../assets/js/appReadTotalBilledInvoicesForPeriodId.js"></script>
<script src="../assets/js/appReadTotalBilledOutvoices.js"></script>
<script src="../assets/js/appReadTotalBilledOutvoicesForPeriodId.js"></script>
<script src="../assets/js/appDeleteInvoices.js"></script>
<script src="../assets/js/appDeleteOutvoices.js"></script>
<script src="../assets/js/appReadTotalDifferenceInvoicesOutvoicesForPeriodId.js"></script>
<script type="text/javascript"></script>
<!-- InputMask -->
<script src="../assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
  $(function () {
    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();
  });




</script>
    <div class="box box-success">
      <div class="modal-body">
         <div class="modal-body">
        <p>¿ Está seguro que desea guardar el movimiento ?</p>
          <button type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
  <script src="../assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="../assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="../assets/plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="../assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../assets/plugins/fastclick/fastclick.js"></script>
<script src="../assets/dist/js/app.min.js"></script>
<script src="../assets/dist/js/pages/dashboard.js"></script>
  <!-- InputMask -->
<script src="../assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>