<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="../LOGOS/gc-favicon.png">
  <title>Consultora | APP</title>
  <link rel='shortcut icon' type='image/png' href='' />
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.css">
  <link rel="stylesheet" href="../assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../assets/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="../assets/plugins/morris/morris.css">
  <link rel="stylesheet" href="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="../assets/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="../assets/plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="../assets/fn/fn_working_popup.css">
</head>



<!--Scripts Work -->
<script src="../assets/js/work-header/googleApisJqueryMins.js"></script>
<script src="../assets/js/work-header/jquery-1.9.1.min.js.js"></script>
<script src="../assets/js/work-header/jquery-2.1.1.min.js.js"></script>
<script type="text/javascript" src="../assets/js/functions/jquery.mask.js"></script>
<script type="text/javascript" src="../assets/js/functions/funciones.js"></script>
<!-- Cookies -->
<script src="../assets/js/functions/cookies.js"></script>
<!-- FN Javascript -->
<script src="../assets/js/config.js"></script>
<script src="../assets/js/APIhelpers.js"></script>
<script src="../assets/js/helpers.js"></script>
<script src="../assets/js/login.js"></script>
<script src="../assets/js/logout.js"></script>
<script src="../assets/js/signup.js"></script>
<script src="../assets/js/invoices.js"></script>
<script src="../assets/js/outvoices.js"></script>
<script src="../assets/js/appReadUserForId.js"></script>
<script src="../assets/js/appReadInvoices.js"></script>
<script src="../assets/js/appReadOutvoices.js"></script>
<script src="../assets/js/appReadTotalBilledInvoices.js"></script>
<script src="../assets/js/appReadTotalBilledInvoicesBiAnnual.js"></script>
<script src="../assets/js/appReadTotalBilledInvoicesAnnual.js"></script>
<script src="../assets/js/appReadTotalBilledInvoicesForPeriodId.js"></script>
<script src="../assets/js/appReadTotalBilledOutvoices.js"></script>
<script src="../assets/js/appReadTotalBilledOutvoicesForPeriodId.js"></script>
<script src="../assets/js/appDeleteInvoices.js"></script>
<script src="../assets/js/appDeleteOutvoices.js"></script>
<script src="../assets/js/appReadTotalDifferenceInvoicesOutvoicesForPeriodId.js"></script>
<script type="text/javascript"></script>
<!-- InputMask -->
<script src="../assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
  $(function () {
    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();
  });

  $(document).ready(function()
  {
    id_period = locationVars('PAR');
    listOutvoices( '37', id_period, fillListOutvoices, failListOutvoices, workingDesignPopupFNCallback );
  });

  function deleteOutvoice( outvoice_id )
  {
    deleteOutvoiceById( '37', outvoice_id, deleteOutvoiceCallback, failDeleteOutvoice, workingDesignPopupFNCallback);

  }

  function fillListOutvoices(result_found, data)
  {
    if (result_found > 0)
    {
        var itemTemplateHTML =   '<tr>'
                                    +'<td scope="col" class="FN_table_cells">|READ_OUTVOICES_PERIODID_DATE|</td>'
                                    +'<td scope="col" class="FN_table_cells">|READ_OUTVOICES_PERIODID_AMOUNT|</td>'
                                    +'<td><button onclick="showUpdateUserPopup(|READ_OUTVOICES_PERIODID_ID|);" type="button" class="btn btn-block btn-warning" style="display:none; width: 50%;" data-toggle="modal" data-target="#divIframeViewElementsActions"><i class="fa fa-fw fa-edit"></i></button><button onclick="deleteOutvoice(|READ_OUTVOICES_PERIODID_ID|);" type="button" class="btn btn-block btn-danger" style="width: 50%;" data-toggle="modal" data-target="#divIframeViewElementsActions"><i class="fa fa-fw fa-minus"></i></button></td>'
                                +'</tr>';
        document.getElementById("contentListTableBody").innerHTML = fillListWithTemplate( data, itemTemplateHTML, fillListOutvoicesListItem );
    }
    else
    {
      document.getElementById("contentListTableBody").innerHTML = '<tr><j style="color:#0073b7; font-weight:bold;">&nbsp; No hay usuarios creados en base de datos</j></tr>';
    }
  }
  
  function deleteOutvoiceCallback(){
    alert("Factura eliminada.");
    listOutvoices( '37', id_period, fillListOutvoices, failListOutvoices, workingDesignPopupFNCallback );
  }

  function failListOutvoices()
  {
    alert("No data loaded in the data base.")   
  }
  function failDeleteOutvoice(){
    alert("Error al borrar Factura.") 
  }
  function saveUserCallback(){
      Alert("Registro creado con éxito.");
  }

  function saveUserError(){
      Alert("Error al grabar.");
  }

  function workingDesignPopupLoginFNCallback(){
        Alert("Estamos trabajando.");
    }

</script>
    <div class="box box-success">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
      </div>
      <div class="modal-body">
        <div class="table-responsive-sm">
              <table class="table">
                <tr>
                  <th>Fecha</th>
                  <th>Monto</th>
                  <th>Borrar</th>
                </tr>
                <!-- <tr>
                  <td>0000-0001</td>
                  <td>01-08-2017</td>
                  <td>19.25</td>
                  <td><button onclick="" type="button" class="btn btn-danger" ><i class="fa fa-fw fa-minus"></i></button>
                  </td>
                </tr> -->
                <tbody id="contentListTableBody"></tbody>
              </table>
            </div>
      </div>
      
    </div>

    <script src="../assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="../assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="../assets/plugins/knob/jquery.knob.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="../assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="../assets/plugins/fastclick/fastclick.js"></script>
<script src="../assets/dist/js/app.min.js"></script>
<script src="../assets/dist/js/pages/dashboard.js"></script>
  <!-- InputMask -->
<script src="../assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>