function addInvoice(userId, period_id, date, invoicePointSale, amount, nr_invoice, successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_invoice_crud_create/create_invoice",
        "userId="+userId+
        "&periodId="+period_id+
        "&date="+date+
        "&invoicePointSale="+invoicePointSale+
        "&amount=" +amount+
        "&numInvoice="+nr_invoice,
        successCallback,
        failCallback,
        workingFNCallback
    );
}