function fillTotalBilledInvoicesForPeriodIdItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['READ_INVOICES_BILLEDFORPERIODID_DATA'];
    var dataBindings =
    {   
        '|READ_INVOICES_BILLEDFORPERIODID_PERIOD_ID|': itemData['READ_INVOICES_BILLEDFORPERIODID_PERIOD_ID'],
        '|READ_INVOICES_BILLEDFORPERIODID_PERIOD_TYPE|': itemDataContent['READ_INVOICES_BILLEDFORPERIODID_PERIOD_TYPE'],
        '|READ_INVOICES_BILLEDFORPERIODID_AMOUNT_TOTAL|': controlResultInvoicesBilledPeriods(itemDataContent['READ_INVOICES_BILLEDFORPERIODID_AMOUNT_TOTAL']),
        '|READ_INVOICES_BILLEDFORPERIODID_USER_ID|': itemDataContent['READ_INVOICES_BILLEDFORPERIODID_USER_ID']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function readTotalBilledInvoicesForPeriodId( userId, periodId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_invoice_read_totalsBilled_periodId/readTotalsBilledInVoicePeriodId",
        "userId="+userId+"&periodId="+periodId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function controlResultInvoicesBilledPeriods ( RESULT )
{
    if( RESULT == null)
    {
        RESULT = "0";
    }
    return(RESULT);
}