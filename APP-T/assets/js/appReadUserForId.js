function fillReadUserForIdItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['USER_DATA'];
    var dataBindings =
    {
        '|EMAIL|': itemDataContent['EMAIL'],
        '|NAME_LAST_NAME|': itemDataContent['NAME_LAST_NAME'],
        '|CUIT|': itemDataContent['CUIT'],
        '|CATEGORY_ID|': itemDataContent['CATEGORY_ID'],
        '|CATEGORY_TYPE|': itemDataContent['CATEGORY_TYPE'],
        '|AMOUNT|': itemDataContent['AMOUNT'],
        '|HIERARCHY_CLIENT|': itemDataContent['HIERARCHY_CLIENT']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function readUserForId( userId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "/app_logged_user_crud_user_read_userid/readUserId",
        "userId="+userId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}