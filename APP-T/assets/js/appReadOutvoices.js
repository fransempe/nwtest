function fillListOutvoicesListItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['READ_OUTVOICES_PERIODID_DATA'];
    var dataBindings =
    {
        '|READ_OUTVOICES_PERIODID_ID|': itemData['READ_OUTVOICES_PERIODID_ID'],
        '|READ_OUTVOICES_PERIODID_PERIOD_ID|': itemDataContent['READ_OUTVOICES_PERIODID_PERIOD_ID'],
        '|READ_OUTVOICES_PERIODID_AMOUNT|': itemDataContent['READ_OUTVOICES_PERIODID_AMOUNT'],
        '|READ_OUTVOICES_PERIODID_DATE|': itemDataContent['READ_OUTVOICES_PERIODID_DATE']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function listOutvoices( userId, id_period, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_outvoice_read_period_id/readOutvoicesForPeriodId",
        "userId="+userId+"&periodId="+id_period,
        successCallback,
        failCallback,
        workingFNCallback
    );
}