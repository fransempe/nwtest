/********APP WORKS HOURS****************/
/*localhost - DEV*/
//var HOST_URL = 'http://localhost/GC/';
var HOST_URL = 'http://nw.efene.xyz/';
/*online*/
//var HOST_URL = 'http://localhost/NWTest/';
/* *******************API URL************************ */
var PROD_URL = 'API/index.php/';
var DEV_URL = 'API/index.php/';
var DEV_MODE = 1;
var BASE_URL = HOST_URL + ( DEV_MODE ? DEV_URL : PROD_URL);

var URL_NAVIGATE = "";
/* *************************************************** */

/*********************GLOBAL VARS************************/
var ALL_TEMPLATES = 0;
var CENTER_COL = 2;
var LEFT_COL = 1;
var RIGHT_COL = 2;
var LEFT_RIGHT_COL = 3;
var DEFAULT = 9;

var USER_PROFILE = 4;
var GRAPHIC = 5;
var CHOOSE_BITS_TYPE = 6;
var CHAT = 7;
var LEADERBOARD = 8;


var SHOW_VIEW_PARAMETER = DEFAULT;

/**************NAME PROJECT********************************/
var PROJECT = 'GC';

/* ********************DEVICES*********************** */
var DEVICE = navigator.userAgent;

/*API RESPONSES*/
var  RESPONSE_SUCCESS = 1;
var RESPONSE_FAILURE = 0;
var FN_WORKING = -1;

var URL_PROJECT = 'http://localhost/NWTest/';

/******PROJECT URL ONLINE'*************/
var PROJECT_URL = URL_PROJECT + 'APP/';