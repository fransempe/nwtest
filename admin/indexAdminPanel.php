<!DOCTYPE html>
<html>
    <head>
    <?php include './linksFiles/commonHeader.php' ?>
    <?php include './linksFiles/commonHeaderElements.php' ?>
        <script>
            $(document).ready(function ()
            {
             loadPage("./views/header.html", "#header", "N");
   
                    /*Admin-User*/
             /*loadPage("./views/login.html", "#content_admin", "Y");*/
             
             loadPage("./views/logged_admin_user_main_menu.html", "#logged_admin_user_main_menu", "Y");
             loadPage("./views/logged_admin_main_menu_left_bar.html", "#logged_admin_user_left_bar", "Y");
             loadPage("./views/logged_admin_user_content.html", "#content_admin", "Y");

             loadPage("./views/footer.html", "#footer", "Y");
            });
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    
        <!-- ==== Header ==== -->
        <div class="main-header" id="header"></div>
        <!-- ==== End Header ==== -->

        <!-- ==== loged admin user menu ==== -->
        <div class="main-header" id="logged_admin_user_main_menu"></div>
        <!-- ==== End Header ==== -->

        <!-- ==== loged admin user bar ==== -->
        <div id="logged_admin_user_left_bar"></div>
        <!-- ==== End Header ==== -->

        <!-- ==== Content ==== -->
        <div class="content-wrapper" id="content_admin"> </div>
        <!-- ==== End Content ==== -->

        <!-- ==== Footer ==== -->
        <div id="footer"></div>
        <!-- ==== End Footer ==== -->

    </body>
    <?php include './linksFiles/commonHeaderScriptsDefaultLayout.php' ?>


<div class="modal fade" id="divIframeViewElementsActions" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <img src="../LOGOS/header-popup-logo.png">
        <h4 style="margin-top: 5px;" class="modal-title" id="formTitle"></h4>
      </div>
      <div class="modal-body">
        <iframe id="iFrameViewElementsActions" class="box" src="" style="height:310px;" scrolling="" frameborder="no"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closePopupElementsActions();" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div id="divIframeView" class="iframe_fn_working" style="display: none;">
      <div class="fn_working_background">
          <div class="box-icon">
              <a onclick="closePopup('divIframeView', 'iFrameView');" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
          </div>
        <iframe id="iFrameView" width="auto" height="auto" src="../FNWORKING/workingFN.html" scrolling="no" frameborder="no"></iframe>
      </div>
</div>

</html>