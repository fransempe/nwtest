var COOKIE_USER_ID_SIGN_UP = "L_USER";
var COOKIE_LOGIN_SIGN_UP = "gc7772018";
var REMEMBER_LOGIN = 0;

function userLogin(userName, userPass,  successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "admin_login/login",
        formatParameters({'userName' : '"'+userName+'"', 'password' : '"'+userPass+'"'}),
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function userTypeIdentificationLogin(userId,  successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_identication_user/user_employee_type",
        formatParameters({'userId' : '"'+userId+'"'}),
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function logOutCallback(successCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "admin_logout/logout",
        formatParameters({}),
        successCallback,
        "",
        workingFNCallback
    );
}

function loginCallback(data)
{
    window.location= ADMIN_PANEL_URL + 'indexAdminPanel.php';
}