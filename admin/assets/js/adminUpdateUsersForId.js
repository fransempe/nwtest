function updateUserForId(userId, userCategoryForHierarchy, userNameLastName, userEmail, userCuit, userPass, successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "admin_logged_user_crud_update/updateUser",
        "userId="+userId+"&categoryForHierarchy="+userCategoryForHierarchy+"&nameLastName="+userNameLastName+"&emailAdress="+userEmail+"&cuit="+userCuit+"&password="+userPass,
        successCallback,
        failCallback,
        workingFNCallback
    );
}