function fillListTotalBilledOutvoicesListItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['READ_OUTVOICES_BILLEDALLPERIODS_DATA'];
    var dataBindings =
    {
        '|READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_ID|': itemData['READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_ID'],
        '|READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_TYPE|': itemDataContent['READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_TYPE'],
        '|READ_OUTVOICES_BILLEDALLPERIODS_AMOUNT_TOTAL|': itemDataContent['READ_OUTVOICES_BILLEDALLPERIODS_AMOUNT_TOTAL'],
        '|READ_OUTVOICES_BILLEDALLPERIODS_USER_ID|': itemDataContent['READ_OUTVOICES_BILLEDALLPERIODS_USER_ID']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function ListTotalBilledOutvoices( userId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_outvoice_read_totalsBilled_AllPeriods/readTotalsBilledOutVoiceAllPeriods",
        "userId="+userId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}