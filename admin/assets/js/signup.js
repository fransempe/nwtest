var COOKIE_USER_ID_SIGN_UP = "L_USER";
var COOKIE_LOGIN_SIGN_UP = "gb7772018";
var REMEMBER_LOGIN = 0;

function signUp(userHierarchy, userName, userNameLastName, userEmail, userBirthday, successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_signup/setUserSignUpData",
        "userHierarchy="+userHierarchy+"&userName="+userName+"&userNameLastName="+userNameLastName+"&userEmail="+userEmail+"&userBirthday="+userBirthday,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function userTypeIdentificationSignUp(userId,  successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "app_identication_user/user_type",
        formatParameters({'userId' : ''+userId+''}),
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function RemoveAccountCallback(successCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "admin_logout/logout",
        formatParameters({}),
        successCallback,
        "",
        workingFNCallback
    );
}

function signUpCallback(data)
{
    window.location= PROJECT_URL + 'app_users.php';
}

function incorrectSignUp(data)
{
    window.location= PROJECT_URL + 'app_users.php';
}