function fillListTotalBilledInvoicesListItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['READ_INVOICES_BILLEDALLPERIODS_DATA'];
    var dataBindings =
    {   
        '|READ_INVOICES_BILLEDALLPERIODS_PERIOD_ID|': itemData['READ_INVOICES_BILLEDALLPERIODS_PERIOD_ID'],
        '|READ_INVOICES_BILLEDALLPERIODS_PERIOD_TYPE|': itemDataContent['READ_INVOICES_BILLEDALLPERIODS_PERIOD_TYPE'],
        '|READ_INVOICES_BILLEDALLPERIODS_AMOUNT_TOTAL|': itemDataContent['READ_INVOICES_BILLEDALLPERIODS_AMOUNT_TOTAL'],
        '|READ_INVOICES_BILLEDALLPERIODS_USER_ID|': itemDataContent['READ_INVOICES_BILLEDALLPERIODS_USER_ID']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function ListTotalBilledInvoices( userId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_invoice_read_totalsBilled_AllPeriods/readTotalsBilledInVoiceAllPeriods",
        "userId="+userId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}