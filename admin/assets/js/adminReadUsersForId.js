function fillReadUsersForIdListItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['USER_DATA'];
    var dataBindings =
    {
        '|ID_USER|': itemData['ID_USER'],
        '|EMAIL|': itemDataContent['EMAIL'],
        '|NAME_LAST_NAME|': itemDataContent['NAME_LAST_NAME'],
        '|CUIT|': itemDataContent['CUIT'],
        '|HIERARCHY_CLIENT|': itemDataContent['HIERARCHY_CLIENT'],
        '|CATEGORY_TYPE|': itemDataContent['CATEGORY_TYPE'],
        '|CATEGORY_ID|': itemDataContent['CATEGORY_ID'],
        '|AMOUNT|': itemDataContent['AMOUNT']
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}


function readUserForId( userId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "admin_logged_user_crud_read_id/readUserForId",
        "userId="+userId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}