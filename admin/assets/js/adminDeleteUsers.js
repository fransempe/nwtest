function deleteUser( userId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "admin_logged_user_crud_delete/deleteUsers",
        "userId="+userId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}