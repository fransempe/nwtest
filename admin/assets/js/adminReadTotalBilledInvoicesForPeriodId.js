function adminFillTotalBilledInvoicesForPeriodIdItem( itemData, itemTemplate )
{
    var itemDataContent = itemData['READ_INVOICES_PERIODID_DATA'];
    var dataBindings =
    {   
        '|READ_INVOICES_PERIODID_ID|': itemData['READ_INVOICES_PERIODID_ID'],
        '|READ_INVOICES_PERIODID_DATE|': itemDataContent['READ_INVOICES_PERIODID_DATE'],
        '|READ_INVOICES_PERIODID_AMOUNT|': itemDataContent['READ_INVOICES_PERIODID_AMOUNT'],
        '|READ_INVOICES_PERIODID_POINT_SALE|': itemDataContent['READ_INVOICES_PERIODID_POINT_SALE'],
        '|READ_INVOICES_PERIODID_NUM_INVOICE|': itemDataContent['READ_INVOICES_PERIODID_NUM_INVOICE'],
        '|READ_INVOICES_PERIODID_PERIOD_ID|': itemDataContent['READ_INVOICES_PERIODID_PERIOD_ID'],
        '|READ_INVOICES_PERIODID_USER_ID|': itemDataContent['READ_INVOICES_PERIODID_USER_ID']
    
    };
    return replaceDataBindings(dataBindings, itemTemplate);
}

function adminReadTotalBilledInvoicesForPeriodId( userId, periodId, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_invoice_read_period_id/readInvoicesForPeriodId",
        "userId="+userId+"&periodId="+periodId,
        successCallback,
        failCallback,
        workingFNCallback
    );
}

function adminControlResultInvoicesBilledPeriods ( RESULT )
{
    if( RESULT == null)
    {
        RESULT = "0";
    }
    return(RESULT);
}