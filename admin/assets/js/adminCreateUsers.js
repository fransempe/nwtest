function createUser(userHierarchy, userCategoryForHierarchy, userNameLastName, emailAdress, cuit, password, successCallback, failCallback, workingFNCallback)
{
    sendAjaxRequest
    (
        "GET",
        "admin_logged_user_crud_create/create_user",
        "hierarchy="+userHierarchy+"&categoryForHierarchy="+userCategoryForHierarchy+"&nameLastName="+userNameLastName+"&emailAdress="+emailAdress+"&cuit="+cuit+"&password="+password,
        successCallback,
        failCallback,
        workingFNCallback
    );
}