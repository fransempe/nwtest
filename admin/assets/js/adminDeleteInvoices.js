function deleteInvoiceById(userId, id_invoice, successCallback, failCallback, workingFNCallback )
{
    sendAjaxRequest
    (
        "GET",
        "app_logged_user_crud_invoice_delete/deleteInvoice",
         "userId="+userId+"&invoiceId="+id_invoice,
        successCallback,
        failCallback,
        workingFNCallback
    );
}