<!DOCTYPE html>
<html>
    <head>
    <?php include './linksFiles/commonHeader.php' ?>
    <?php include './linksFiles/commonHeaderElements.php' ?>
        <script>
            $(document).ready(function ()
            {
                loadPage("./views/header.html", "#header", "N");
   
                    /*Admin-User*/
                loadPage("./views/login.html", "#content_admin", "Y");
             

                loadPage("./views/footer.html", "#footer", "Y");
            });
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    
        <!-- ==== Header ==== -->
        <div id="header"></div>
        <!-- ==== End Header ==== -->

        <!-- ==== Content ==== -->
        <div id="content_admin"> </div>
        <!-- ==== End Content ==== -->

        <!-- ==== Footer ==== -->
        <div id="footer"></div>
        <!-- ==== End Footer ==== -->

    </body>
    <?php include './linksFiles/commonHeaderScriptsDefaultLayout.php' ?>
</html>