<!-- FN Javascript -->
<script src="./assets/js/config.js"></script>
<script src="./assets/js/APIhelpers.js"></script>
<script src="./assets/js/helpers.js"></script>
<script src="./assets/js/signup.js"></script>
<script src="./assets/js/adminCreateUsers.js"></script>
<script src="./assets/js/adminReadUsers.js"></script>
<script src="./assets/js/adminReadUsersForId.js"></script>
<script src="./assets/js/adminUpdateUsersForId.js"></script>
<script src="./assets/js/adminListUsers.js"></script>
<script src="./assets/js/adminDeleteUsers.js"></script>
<script src="./assets/js/adminReadTotalBilledInvoices.js"></script>
<script src="./assets/js/adminReadTotalBilledOutvoices.js"></script>
<script src="./assets/js/adminReadTotalBilledInvoicesForPeriodId.js"></script>
<script src="./assets/js/adminReadTotalBilledOutvoicesForPeriodId.js"></script>
<script src="./assets/js/adminDeleteInvoices.js"></script>
<script src="./assets/js/adminDeleteOutvoices.js"></script>
<script type="text/javascript"></script>
