<?php include '../linksFiles/popups-commonHeader.php' ?>
<?php include '../linksFiles/popups-commonHeaderElements.php' ?>
<!-- InputMask -->
<script src="../assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
  $(function () {

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();
  });

  function updateUser()
  {
      var userId = document.getElementById('userId').value;
      var userCategoryForHierarchy =  document.getElementById('userCategoryForHierarchy').value;
      var userNameLastName =  document.getElementById('userNameLastName').value;
      var userEmail =  document.getElementById('userEmail').value;
      var userCuit =  document.getElementById('userCuit').value;
      var userPass =  document.getElementById('userPass').value;
      disabledInputsDataFieldsPopupUpdateUsers();
      updateUserForId( userId, userCategoryForHierarchy, userNameLastName, userEmail, userCuit, userPass, updateUserCallback, updateUserError, workingDesignPopupFNCallback );
  }

function updateUserCallback( result_found, data )
{
    var position = 0;
    if (result_found > 1)
    {
      position = result_found - 1
    }
    var message = data['content'][position]['message'];
    document.getElementById('updateUserResponseMessage').innerHTML = message;
    document.getElementById('updateUserResponseMessage').style.color = '#0d7ed6';
    visibilityElement('updateUserResponseMessage');
    setTimeout(enabledInputsDataFieldsPopupUpdateUsers, 3000);
}

  function updateUserError( data )
{
    var message = data['message'];
    document.getElementById('updateUserResponseMessage').innerHTML = message;
    document.getElementById('updateUserResponseMessage').style.color = '#F45155';
    visibilityElement('updateUserResponseMessage');
    setTimeout(enabledInputsDataFieldsPopupUpdateUsers, 3000);
}

  function fillPopupAdminUpdateUser( result_found, data )
  {
    var userId = data['content'][0]['ID_USER'];
    var userCategoryForHierarchy = data['content'][0]['USER_DATA']['CATEGORY_ID'];
    var userNameLastName = data['content'][0]['USER_DATA']['NAME_LAST_NAME'];
    var userEmail = data['content'][0]['USER_DATA']['EMAIL'];
    var userCuit = data['content'][0]['USER_DATA']['CUIT'];
    document.getElementById('userId').value = userId;
    document.getElementById('userCategoryForHierarchy').value = userCategoryForHierarchy;
    document.getElementById('userNameLastName').value = userNameLastName;
    document.getElementById('userEmail').value = userEmail;
    document.getElementById('userCuit').value = userCuit;
    enabledInputsDataFieldsPopupUpdateUsers();
  }

  function failPopupAdminUpdateUser( data )
  {
     var message = data['message'];
     document.getElementById('deleteUserResponseMessage').innerHTML = message;
     showElement('deleteUserResponseMessage');
  }

  function obtainParameterPopupUpdate()
  {
     var URLactual = window.location;
     var userIdParameter = getAllUrlParams(URL_POPUP).uu; 
     readUserForId( userIdParameter, fillPopupAdminUpdateUser, failPopupAdminUpdateUser, workingDesignPopupFNCallback );
  }

  function enabledInputsDataFieldsPopupUpdateUsers()
  {
    document.getElementById('userCategoryForHierarchy').disabled = false;
    document.getElementById('userNameLastName').disabled = false;
    document.getElementById('userEmail').disabled = false;
    document.getElementById('userCuit').disabled = false;
    document.getElementById("buttonSetDataEditUser").disabled = false;
  }

  function disabledInputsDataFieldsPopupUpdateUsers()
  {
    document.getElementById('userCategoryForHierarchy').disabled = true;
    document.getElementById('userNameLastName').disabled = true;
    document.getElementById('userEmail').disabled = true;
    document.getElementById('userCuit').disabled = true;
    document.getElementById("buttonSetDataEditUser").disabled = true;
  }

  function emptyInputsDataFieldPopupUpdateUsers()
  {
    document.getElementById('userCategoryForHierarchy').value = '';
    document.getElementById('userNameLastName').value = '';
    document.getElementById('userEmail').value = '';
    document.getElementById('userCuit').value = '';
  }

$(document).ready(function()
{
  disabledInputsDataFieldsPopupUpdateUsers();
  emptyInputsDataFieldPopupUpdateUsers();
  obtainParameterPopupUpdate();
});

</script>
    <div class="box-body">
      <input id="userId" style="display: none;">
      <br>
      <label>Seleccione categoría</label>
      <select id="userCategoryForHierarchy" class="form-control select2" style="width: 50%;">
        <option value="1">A</option>
        <option value="2">B</option>
        <option value="3">C</option>
        <option value="4">D</option>
        <option value="5">E</option>
        <option value="6">F</option>
        <option value="7">G</option>
        <option value="8">H</option>
        <option value="9">I</option>
        <option value="10">J</option>
        <option value="11">K</option>
      </select>
      <br>
      Apellido y Nombre
      <input required id="userNameLastName" class="form-control" type="text" placeholder="Apellido y Nombre">
      <br>
      EMail
      <input required id="userEmail" class="form-control" type="text" placeholder="EMail">
      <br>
      Contraseña
      <input required id="userPass" class="form-control" type="text" placeholder="">
      <br>
      <label>Cuit:</label>
      <br>
      <input id="userCuit" class="userCuit" placeholder="" style="padding-left: 10px; height: 33px;">
      <br>
      <br>
      <div id="updateUserResponseMessage" align="center" style="visibility:hidden; color: #F45155; font-size:18px; margin-top: 10px;">Message</div>
    </div>
    <!-- /.box-body -->
  <div class="modal-footer">
    <button id="buttonSetDataEditUser" type="button" class="btn btn-primary" onclick="updateUser();">Modificar Usuario</button>
  </div>
<?php include '../linksFiles/popups-commonHeaderScriptsDefaultLayout.php' ?>