<?php include '../linksFiles/popups-commonHeader.php' ?>
<?php include '../linksFiles/popups-commonHeaderElements.php' ?>
<!-- InputMask -->
<script src="../assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>

  $(function () {

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();
  });



  function SwitchDetailsPopups()
  {
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    
    if(selectedValue=='ventas'){
     document.getElementById("iFrameViewElementsDetails").src = PROJECT_URL+'popup/logged_admin_user_popup_users_crud_read_totals_invoices.php?PAR='+id_user;
     // showElement("iFrameViewElementsDetails");

      } else {
       document.getElementById("iFrameViewElementsDetails").src = PROJECT_URL+'popup/logged_admin_user_popup_users_crud_read_totals_outvoices.php?PAR='+id_user;
        // showElement("IframeViewElementsDetails");
      }
  }


var id_user = '';
  $(document).ready(function()
  {
    id_user = locationVars('uu');
    //listInvoices( readCookie(COOKIE_USER_ID), id_period, fillListInvoices, failListInvoices, workingDesignPopupFNCallback );
  });

</script>
    <div class="form-group">
      <label>Tipo Facturación</label>
      <select id="selectBox" onclick="SwitchDetailsPopups();" class="form-control select2"  style="width: 100%;">
        <option value="ventas">Ventas</option>
        <option value="compras">Compras</option>
      </select>
    </div>
<iframe id="iFrameViewElementsDetails" class="box" src="" style="height:310px;" scrolling="" frameborder="no"></iframe>

<?php include '../linksFiles/popups-commonHeaderScriptsDefaultLayout.php' ?>