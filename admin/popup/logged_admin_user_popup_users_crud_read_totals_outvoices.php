<?php include '../linksFiles/popups-commonHeader.php' ?>
<?php include '../linksFiles/popups-commonHeaderElements.php' ?>
<!-- InputMask -->
<script src="../assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
  
$(function () {
    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();
  });

var id_user = '';
$(document).ready(function()
{
  id_user = locationVars('PAR');
 // emptyInputsDataFieldPopupUpdateUsers();
  //obtainParameterPopupUpdate();

  ListTotalBilledOutvoices( id_user,fillListTotalBilledOutvoices, failListTotalBilled, workingDesignPopupFNCallback );
});

function fillListTotalBilledOutvoices(result_found, data)
  {
    if (result_found > 0)
    {
        closeElement('outvoicesTableDetails');
        showElement('outvoicesTable');

        var itemTemplateHTML =   '<tr>'
                                    +'<td scope="col" class="FN_table_cells">|READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_TYPE|</td>'
                                    +'<td scope="col" class="FN_table_cells">|READ_OUTVOICES_BILLEDALLPERIODS_AMOUNT_TOTAL|</td>'
                                    +'<td><button onclick="showUsersPeriodDetails(|READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_ID|);" type="button" class="btn btn-Primary" data-toggle="modal" data-target="#divIframeViewElementsActions"><i class="fa fa-fw fa-eye"></i></button></td>'
                                +'</tr>';
        
        
        document.getElementById("contentListTableOutvoicesBody").innerHTML = fillListWithTemplate( data, itemTemplateHTML, fillListTotalBilledOutvoicesListItem );
    }
    else
    {
      document.getElementById("contentListTableOutvoicesBody").innerHTML = '<tr><j style="color:#0073b7; font-weight:bold;">&nbsp; No hay registros creados en base de datos</j></tr>';
    }
  }

function showUsersPeriodDetails(period_id){
  adminReadTotalBilledOutvoicesForPeriodId( id_user, period_id,fillListTotalBilledOutvoicesForPeriodId, failListTotalBilled, workingDesignPopupFNCallback );
}

function fillListTotalBilledOutvoicesForPeriodId(result_found, data){
 if (result_found > 0)
    {
        closeElement('outvoicesTable');
        showElement('outvoicesTableDetails');
        var itemTemplateHTML = '<tr>'
                                    +'<td scope="col" class="FN_table_cells">|READ_OUTVOICES_PERIODID_DATE|</td>'
                                    +'<td scope="col" class="FN_table_cells">|READ_OUTVOICES_PERIODID_AMOUNT|</td>'
                                    +'<td><button onclick="deleteOutvoice(|READ_OUTVOICES_PERIODID_ID|);" type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#divIframeViewElementsActions"><i class="fa fa-fw fa-minus"></i></button></td>'
                                +'</tr>';

        document.getElementById("contentListTableOutvoicesBodyDetails").innerHTML = fillListWithTemplate( data, itemTemplateHTML, adminFillTotalBilledOutvoicesForPeriodIdItem );
    }
    else
    {
      document.getElementById("contentListTableOutvoicesBodyDetails").innerHTML = '<tr><j style="color:#0073b7; font-weight:bold;">&nbsp; No hay registros creados en base de datos</j></tr>';
    }
}

function deleteOutvoice(outvoice_id)
  {
    deleteOutvoiceById( id_user, outvoice_id, deleteOutvoiceCallback, failDeleteOutvoice, workingDesignPopupFNCallback );
  }

function deleteOutvoiceCallback(){
    alert("Factura eliminada.");
    ListTotalBilledOutvoices( id_user,fillListTotalBilledOutvoices, failListTotalBilled, workingDesignPopupFNCallback );
  }


function failDeleteOutvoice(){
    alert("Error al borrar Factura.") 
  }

function failListTotalBilled(){
      alert("No data loaded in the data base.")   
  }

function workingDesignPopupLoginFNCallback(){
      alert("Estamos trabajando.");
  }


</script>
    
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div id="outvoicesTable" class="table-responsive-sm" style="display: none;">
              <table class="table">
                <tr>
                  <th>Periodo</th>
                  <th>Total ($)</th>
                  <th>Ver detalles</th>
                </tr>
                <tbody id="contentListTableOutvoicesBody"></tbody>
              </table>
            </div>
            <div id="outvoicesTableDetails" class="table-responsive-sm" style="display: none;">
               <table class="table">
                <tr>
                  <th>Fecha</th>
                  <th>Monto</th>
                </tr>
                <tbody id="contentListTableOutvoicesBodyDetails"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php include '../linksFiles/popups-commonHeaderScriptsDefaultLayout.php' ?>