<?php include '../linksFiles/popups-commonHeader.php' ?>
<?php include '../linksFiles/popups-commonHeaderElements.php' ?>

<script>

function setDataAddUser()
{
  var hierarchy =  document.getElementById('userHierarchy').value;
  var userCategoryForHierarchy =  document.getElementById('userCategoryForHierarchy').value;
  var userNameLastName =  document.getElementById('userNameLastName').value;
  var userEmail =  document.getElementById('userEmail').value;
  var cuit =  document.getElementById('userCuit').value;
  var password =  document.getElementById('userPassword').value;
  document.getElementById("buttonSetDataAddUser").disabled = true;
  createUser( userHierarchy, userCategoryForHierarchy, userNameLastName, userEmail, cuit, password, addUserCallback, addUserError, workingDesignPopupLoginFNCallback );
}

function addUserCallback( result_found, data )
{
    var message = data['content'][0]['message'];
    document.getElementById('createUserResponseMessage').innerHTML = message;
    document.getElementById('createUserResponseMessage').style.color = '#0d7ed6';
    visibilityElement('createUserResponseMessage');
    setTimeout(addUserRemoveDataFields, 3000);
}

function addUserRemoveDataFields()
{
    document.getElementById('userHierarchy').value = "Monotributo";
    document.getElementById('userCategoryForHierarchy').value = "A";
    document.getElementById('userNameLastName').value = "";
    document.getElementById('userEmail').value = "";
    document.getElementById('userCuit').value = "";
    document.getElementById('userPassword').value = "";
    hiddenElement('createUserResponseMessage');
    document.getElementById('createUserResponseMessage').style.color = '#F45155';
    document.getElementById("buttonSetDataAddUser").disabled = false;
}

function addUserError( data )
{
    var message = data['message'];
    document.getElementById('createUserResponseMessage').innerHTML = message;
    visibilityElement('createUserResponseMessage');
    document.getElementById("buttonSetDataAddUser").disabled = false;
}

function workingDesignPopupLoginFNCallback()
{
      Alert("Estamos trabajando.");
}


$(document).ready(function(){
    $("#userCuit").inputmask({"mask": "99-99999999-9"});
});

</script>
  <div class="modal-body">
    <div class="form-group">
      <label>Seleccione jerarquía</label>
      <select id="userHierarchy" class="form-control select2" style="width: 50%;">
        <option value="3" selected="selected">Monotributo</option>
      </select>
      <br>
      <label>Seleccione categoría</label>
      <select id="userCategoryForHierarchy" class="form-control select2" style="width: 50%;">
        <option value="1" selected="selected">A</option>
        <option value="2">B</option>
        <option value="3">C</option>
        <option value="4">D</option>
        <option value="5">E</option>
        <option value="6">F</option>
        <option value="7">G</option>
        <option value="8">H</option>
        <option value="9">I</option>
        <option value="10">J</option>
        <option value="11">K</option>
      </select>
      <br>
      <label>Apellido y Nombre:</label>
      <input id="userNameLastName" type="text" class="form-control" placeholder="">
      <br>
      <label>Email:</label>
      <input id="userEmail" type="text" class="form-control" placeholder="">
      <br>
      <label>Cuit:</label>
      <br>
      <input id="userCuit" class="userCuit" placeholder="" style="height: 33px;">

      <br>
      <br>
      <label>Password:</label>
      <br>
      <input id="userPassword" type="password" style="height: 33px;">
      <br>
      <div id="createUserResponseMessage" align="center" style="visibility:hidden; color: #F45155; font-size:18px; margin-top: 10px;">Message</div>
    </div>


  <div class="modal-footer">
    <button id="buttonSetDataAddUser" type="button" class="btn btn-primary" onclick="setDataAddUser();">Agregar usuario</button>
  </div>
</div>

<?php include '../linksFiles/popups-commonHeaderScriptsDefaultLayout.php' ?>