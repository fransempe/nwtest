<?php include '../linksFiles/popups-commonHeader.php' ?>
<?php include '../linksFiles/popups-commonHeaderElements.php' ?>
<!-- InputMask -->
<script src="../assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>

  function fillDeleteUser( result_found, data )
  {
      var message = data['content'][0]['message'];
      document.getElementById('deleteUserResponseMessage').innerHTML = message;
      closeElement('deleteUserMessagePrincipal');
      closeElement('deleteUserButton');
      showElement('deleteUserResponseMessage');
  }

  function failDeleteUser( data )
  {
     var message = data['message'];
     document.getElementById('deleteUserResponseMessage').innerHTML = message;
     showElement('deleteUserResponseMessage');
  }

  function obtainParameterPopupDelete()
  {
     var URLactual = window.location;
     var userIdParameter = getAllUrlParams(URL_POPUP).uu; 
     deleteUser( userIdParameter, fillDeleteUser, failDeleteUser, workingDesignPopupFNCallback ); 
  }

</script>
           
    <div class="box-body" style="border-top-color:#dd4b39;">
      <j id="deleteUserMessagePrincipal">¿Está seguro que desea borrar al usuario seleccionado?</j>
      <div id="deleteUserResponseMessage" align="center" style="display: none; color: #F45155; font-size:18px; margin-top: 10px;">Message</div>
    </div>
    <!-- /.box-body -->
    <div class="modal-footer">
      <button id="deleteUserButton" type="button" class="btn btn-primary" onclick="obtainParameterPopupDelete();">Borrar Usuario</button>
    </div>
<?php include '../linksFiles/popups-commonHeaderScriptsDefaultLayout.php' ?>