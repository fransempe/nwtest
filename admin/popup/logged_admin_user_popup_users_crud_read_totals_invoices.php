<?php include '../linksFiles/popups-commonHeader.php' ?>
<?php include '../linksFiles/popups-commonHeaderElements.php' ?>
<!-- InputMask -->
<script src="../assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
  $(function () {

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();
  });
  
var id_user = '';
$(document).ready(function()
{
  id_user = locationVars('PAR');

  ListTotalBilledInvoices( id_user,fillListTotalBilledInvoices, failListTotalBilled, workingDesignPopupFNCallback );
});


 function fillListTotalBilledInvoices(result_found, data)
  {
    if (result_found > 0)
    {
        closeElement('invoicesTableDetails');
        showElement('invoicesTable');

        var itemTemplateHTML =   '<tr>'
                                    +'<td scope="col" class="FN_table_cells">|READ_INVOICES_BILLEDALLPERIODS_PERIOD_TYPE|</td>'
                                    +'<td scope="col" class="FN_table_cells">|READ_INVOICES_BILLEDALLPERIODS_AMOUNT_TOTAL|</td>'
                                    +'<td><button onclick="showUsersPeriodDetails(|READ_INVOICES_BILLEDALLPERIODS_PERIOD_ID|);" type="button" class="btn btn-Primary" data-toggle="modal" data-target="#divIframeViewElementsActions"><i class="fa fa-fw fa-eye"></i></button></td>'
                                +'</tr>';
        
        
        document.getElementById("contentListTableInvoicesBody").innerHTML = fillListWithTemplate( data, itemTemplateHTML, fillListTotalBilledInvoicesListItem );
    }
    else
    {
      document.getElementById("contentListTableInvoicesBody").innerHTML = '<tr><j style="color:#0073b7; font-weight:bold;">&nbsp; No hay registros creados en base de datos</j></tr>';
    }
  }

function showUsersPeriodDetails(period_id){
  adminReadTotalBilledInvoicesForPeriodId( id_user, period_id,fillListTotalBilledInvoicesForPeriodId, failListTotalBilled, workingDesignPopupFNCallback );
}

function fillListTotalBilledInvoicesForPeriodId(result_found, data){
 if (result_found > 0)
    {
        closeElement('invoicesTable');
        showElement('invoicesTableDetails');
        var itemTemplateHTML = '<tr>'
                                    +'<td scope="col" class="FN_table_cells">|READ_INVOICES_PERIODID_DATE|</td>'
                                    +'<td scope="col" class="FN_table_cells">|READ_INVOICES_PERIODID_AMOUNT|</td>'
                                    +'<td scope="col" class="FN_table_cells">|READ_INVOICES_PERIODID_POINT_SALE|</td>'
                                    +'<td scope="col" class="FN_table_cells">|READ_INVOICES_PERIODID_NUM_INVOICE|</td>'
                                    +'<td><button onclick="deleteInvoice(|READ_INVOICES_PERIODID_ID|);" type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#divIframeViewElementsActions"><i class="fa fa-fw fa-minus"></i></button></td>'
                                +'</tr>';

        document.getElementById("contentListTableInvoicesBodyDetails").innerHTML = fillListWithTemplate( data, itemTemplateHTML, adminFillTotalBilledInvoicesForPeriodIdItem );
    }
    else
    {
      document.getElementById("contentListTableInvoicesBodyDetails").innerHTML = '<tr><j style="color:#0073b7; font-weight:bold;">&nbsp; No hay registros creados en base de datos</j></tr>';
    }
}

function deleteInvoice(invoice_id)
  {
    deleteInvoiceById( id_user, invoice_id, deleteInvoiceCallback, failDeleteInvoice, workingDesignPopupFNCallback );
  }

function failListTotalBilled(){
      alert("No data loaded in the data base.")   
  }

function workingDesignPopupLoginFNCallback(){
      alert("Estamos trabajando.");
  }

function deleteInvoiceCallback(){
    alert("Factura eliminada.");
    ListTotalBilledInvoices( id_user,fillListTotalBilledInvoices, failListTotalBilled, workingDesignPopupFNCallback );
  }

function failDeleteInvoice(){
    alert("Error al borrar Factura.") 
  }
</script>

    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <div class="table-responsive-sm">
              <div id="invoicesTable" class="table-responsive-sm" style="display: none;">
                <table class="table">
                  <tr>
                    <th>Periodo</th>
                    <th>Total ($)</th>
                    <th>Ver detalles</th>
                  </tr>
                  <tbody id="contentListTableInvoicesBody"></tbody>
                </table>
              </div>
              <div id="invoicesTableDetails" class="table-responsive-sm" style="display: none;">
                 <table class="table">
                  <tr>
                    <th>Fecha</th>
                    <th>Monto</th>
                    <th>Pto. Venta</th>
                    <th>Nº Factura</th>
                  </tr>
                  <tbody id="contentListTableInvoicesBodyDetails"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<?php include '../linksFiles/popups-commonHeaderScriptsDefaultLayout.php' ?>