<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_outvoice_delete
{  
    public function deleteOutvoice()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryDeleteOutvoices';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        if ( isset($_GET['outvoiceId']) && $_GET['outvoiceId'] != '' )
        {
            $outvoiceId = $_GET['outvoiceId'];
        }else
            isRequired( 'Id de factura de compra' );

        $PARAMETERS = array( $USER_ID, $outvoiceId );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_outvoice_delete', 'deleteOutvoiceJson', $PARAMETERS );
    }

    public function deleteOutvoiceJson( $QUERY_DATA_BASE, $DETAIL_COMPANY, $PARAMETERS )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Factura de compra borrada correctamente.',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
    }
}

?>