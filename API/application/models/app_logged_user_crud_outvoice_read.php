<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_outvoice_read
{  
    public function readOutvoices()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadOutvoices';

        $PARAMETERS = array( '' );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_outvoice_read', 'readOutvoicesJson', $PARAMETERS );
    }

    function readOutvoicesJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            while ( $readOutvoicesJson = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'READ_OUTVOICES_ID' => $readOutvoicesJson['id'],
                    'READ_OUTVOICES_DATA' => array(
                        'READ_OUTVOICES_DATE' => $readOutvoicesJson['date'],
                        'READ_OUTVOICES_AMOUNT' => $readOutvoicesJson['amount'],
                        'READ_OUTVOICES_PERIOD_ID' => $readOutvoicesJson['id_period'],
                        'READ_OUTVOICES_USER_ID' => $readOutvoicesJson['id_user']
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }
}

?>