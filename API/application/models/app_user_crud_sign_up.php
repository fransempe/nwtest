<?php

include_once(dirname(__FILE__).'./../config/config.php');

class app_user_crud_sign_up
{   
    public function set_sign_up()
    {
        $COMPANY = company::COMPANY_DETAIL;

        if ( isset($_GET['name']) )
        {
            $name = $_GET['name'];
        }else
            isRequired( 'Name' );

        if ( isset($_GET['userName']) )
        {
            $username = $_GET['userName'];
        }else
            isRequired( 'User Name' );

        if ( isset($_GET['emailAdress']) )
        {
            $emailAdress = $_GET['emailAdress'];
        }else
            isRequired( 'Email Adress' );

        $validateMailResult = email_validate_short( $emailAdress );

        if ( $validateMailResult == "0" )
        {
            validateMail();
        }

        if ( isset($_GET['password']) )
        {
            $USER_PASS = $_GET['password']; 
        }else
            isRequired( 'Password' );

        if ( isset($_GET['rePassword']) )
        {
            $USER_RE_PASS = $_GET['rePassword'];    
        }else
            isRequired( 'Re Password' );

        if ( $USER_PASS != $USER_RE_PASS )
        {
            passwordDoNotMatch();
        }

        if ( $name == '' || $username == '' || $emailAdress == '' || $USER_PASS == '' || $USER_RE_PASS == '' )
        {
            missingData( 'incorrect data,' );
        }

        if( $USER_PASS != '' )
        {
           $USER_PASS = encrypt($USER_PASS, credentials::USER_PATRON_PASS);
        }

        $USER_PASS_SECRET_CURRENT_TIME_LOG_OUT = 'userFN8080';

        $hierarchy_id = 3;

        $data_profile_id = 0;

        $USER_PASS_SECRET_CANCELED_ACCOUNTS = 'userFN8080';

        $PARAMETERS = array( $USER_PASS_SECRET_CURRENT_TIME_LOG_OUT, $name, $username, $emailAdress, $USER_PASS, $hierarchy_id, $data_profile_id, $USER_PASS_SECRET_CANCELED_ACCOUNTS );
        //print_r( $PARAMETERS );

        $QUERY_CONTROL_FOR_NEW_USER_EMAIL = 'queryControlNewUserMailsSearch';
        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_CONTROL_FOR_NEW_USER_EMAIL, 'app_sign_up', 'controlResultNewUserMail', $PARAMETERS );
    }

    public function controlResultNewUserMail( $QUERY_DATA_BASE, $DETAIL_COMPANY, $PARAMETERS )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        while ( $emailController = mysql_fetch_array( $QUERY_DATA_BASE ) )
        {
            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'ID_USER' => $emailController['id'],
                'USER_DATA' => array(
                    'email' => $emailController['email']
                ),
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
        } 
        if ( $AMOUNT_QUERY_RESULTS == 1 )
        {
            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Account created successfully',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $COMPANY = company::GOLD_BITS_COMPANY;
            $QUERY_TABLE = '';
            $connect = new connect();
            $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_sign_up', 'appSignUpJson', $PARAMETERS );  
            exit();
        }
        else
        {
            $COMPANY = company::GOLD_BITS_COMPANY;
            $QUERY_TABLE = 'insertUserApp';
            $connect = new connect();
            $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_sign_up', 'appSignUpJson', $PARAMETERS );  
        }
    }

    public function appSignUpJson( $QUERY_DATA_BASE, $DETAIL_COMPANY, $PARAMETERS )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Account created successfully',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
    }
}

?>