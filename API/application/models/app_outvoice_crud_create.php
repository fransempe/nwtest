<?php

include_once(dirname(__FILE__).'./../config/config.php');

class app_outvoice_crud_create
{   
    public function create_outvoice()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'create_outvoice';

        if ( isset($_GET['userId']) )
        {
            $USER_ID = $_GET['userId']; 
        }else
            isRequired( 'Id de usuario' );

        if ( isset($_GET['periodId']) )
        {
            $periodId = $_GET['periodId']; 
        }else
            isRequired( 'Periodo' );

        if ( isset($_GET['date']) )
        {
            $date = $_GET['date'];
        }else
            isRequired( 'Fecha de ingreso' );

        if ( isset($_GET['amount']) )
        {
            $amount = $_GET['amount'];
        }else
            isRequired( 'Monto total' );

        if ( $USER_ID == '' || $date == '' || $amount == '' || $periodId == '' )
        {
            missingData( 'incorrect data,' );
        }

        $PARAMETERS = array( $USER_ID, $periodId, $date, $amount );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_outvoice_crud_create', 'appAddOutvoiceJson', $PARAMETERS );
    }

    public function appAddOutvoiceJson( $QUERY_DATA_BASE, $DETAIL_COMPANY, $PARAMETERS )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Outvoice created successfully',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
    }
}

?>