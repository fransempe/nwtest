<?php

include_once(dirname(__FILE__).'./../config/config.php');

class admin_logged_user_crud_create
{   
    public function create_user()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'addUserApp';

        if ( isset($_GET['categoryForHierarchy']) && $_GET['categoryForHierarchy'] != '' )
        {
            $categoryForHierarchy_id = $_GET['categoryForHierarchy'];
        }else
            isRequired( 'Categoria' );

        if ( isset($_GET['nameLastName']) && $_GET['nameLastName'] != '' )
        {
            $nameLastName = $_GET['nameLastName'];
        }else
            isRequired( 'Apellido y Nombre' );

        if ( isset($_GET['emailAdress']) && $_GET['emailAdress'] != '' )
        {
            $emailAdress = $_GET['emailAdress'];
        }else
            isRequired( 'Email' );

        $validateMailResult = email_validate_short( $emailAdress );

        if ( $validateMailResult == "0" )
        {
            validateMail();
        }

        if ( isset($_GET['cuit']) && $_GET['cuit'] != '' )
        {
            $cuit = $_GET['cuit'];
        }else
            isRequired( 'cuit' );

        if ( isset($_GET['password']) && $_GET['password'] != '' )
        {
            $USER_PASS = $_GET['password']; 
        }else
            isRequired( 'La clave de seguridad' );

        if( $USER_PASS != '' )
        {
           $USER_PASS = encrypt($USER_PASS, credentials::USER_PATRON_PASS);
        }

        $USER_PASS_SECRET_CURRENT_TIME_LOG_OUT = 'userFN8080';

        $hierarchy_id = 3;

        $hierarchy_client_id = 1;

        $data_profile_id = 0;

        $USER_PASS_SECRET_CANCELED_ACCOUNTS = 'userFN8080';

        $PARAMETERS = array( $USER_PASS_SECRET_CURRENT_TIME_LOG_OUT, $nameLastName, $cuit, $emailAdress, $USER_PASS, $hierarchy_id, $categoryForHierarchy_id, $hierarchy_client_id, $data_profile_id, $USER_PASS_SECRET_CANCELED_ACCOUNTS );

        //print_r($PARAMETERS);
        //die();

        $QUERY_CONTROL_FOR_NEW_USER_EMAIL = 'queryControlNewUserMailsSearch';
        
        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_CONTROL_FOR_NEW_USER_EMAIL, 'admin_logged_user_crud_create', 'controlResultNewUserMail', $PARAMETERS );
    }


    public function controlResultNewUserMail( $QUERY_DATA_BASE, $DETAIL_COMPANY, $PARAMETERS )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        $emailCheck = '';
        
        while ( $emailController = mysql_fetch_array( $QUERY_DATA_BASE ) )
        {
            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'ID_USER' => $emailController['id'],
                'USER_DATA' => array(
                    'email' => $emailController['email']
                ),
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $emailCheck = $emailController['email'];
            $AMOUNT_QUERY_RESULTS++;
        } 

        if ( $AMOUNT_QUERY_RESULTS == 1 )
        {
            $COMPANY = company::COMPANY_DETAIL;
            userIsAlreadyCreatedWithThisMail( 'The user is already created with this mail', $emailCheck  );
        }
        else
        {
            $COMPANY = company::COMPANY_DETAIL;
            $QUERY_TABLE = 'addUserApp';
            $connect = new connect();
            $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'admin_logged_user_crud_create', 'adminCreatedUserJson', $PARAMETERS );  
        }
    }

    public function adminCreatedUserJson( $QUERY_DATA_BASE, $DETAIL_COMPANY, $PARAMETERS )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Usuario creado correctamente.',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
    }
}

?>