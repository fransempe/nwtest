<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_invoice_read_totalsBilled_periodId
{  
    public function readTotalsBilledInVoicePeriodId()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadInvoicesTotalsBilledForPeriodId';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        if ( isset($_GET['periodId']) && $_GET['periodId'] != '' )
        {
            $periodId = $_GET['periodId'];
        }else
            isRequired( 'Id de periodo' );

        $PARAMETERS = array( $USER_ID, $periodId );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_invoice_read_totalsBilled_periodId', 'readTotalsBilledPeriodIdJson', $PARAMETERS );
    }

    function readTotalsBilledPeriodIdJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            while ( $readTotalsBilledPeriodIdJson = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'READ_INVOICES_BILLEDFORPERIODID_PERIOD_ID' => $readTotalsBilledPeriodIdJson['period_id'],
                    'READ_INVOICES_BILLEDFORPERIODID_DATA' => array(
                        'READ_INVOICES_BILLEDFORPERIODID_PERIOD_TYPE' => $readTotalsBilledPeriodIdJson['period_type'],
                        'READ_INVOICES_BILLEDFORPERIODID_AMOUNT_TOTAL' => $readTotalsBilledPeriodIdJson['total'],
                        'READ_INVOICES_BILLEDFORPERIODID_USER_ID' => $readTotalsBilledPeriodIdJson['id_user']
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }           
}
?>