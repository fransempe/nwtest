<?php

include_once(dirname(__FILE__).'./../config/config.php');

class admin_user_login
{   
    public function admin_login()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'sqlQueryUserLogin';
        
        if ( isset($_GET['userName']) )
        {
            $username = $_GET['userName'];
        }else
            isRequired( 'userName' );

        if ( isset($_GET['password']) )
        {
            $USER_PASS = $_GET['password'];
            //die($USER_PASS);
            /*$USER_PASS = encrypt($USER_PASS, credentials::USER_PATRON_PASS);*/
        }else
            isRequired( 'password' );

        $PARAMETERS = array( $username, $USER_PASS );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'admin_login', 'adminLoginJson', $PARAMETERS );
    }

    public function adminLoginJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $APP_ID_DETECTED;
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        //hardware_identification_by_call_type( config::IDENTIFICATION_HARDWARE_LOGIN );

        if (isset($QUERY_DATA_BASE))
        {
            while($user = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $loginUser {'success'} = 1;
                $loginUser{'data'}{'content'}
                [$AMOUNT_QUERY_RESULTS] = array(
                    'ID_USER'  => $user['id'],
                    'USER_NAME'=> $user['username'],
                    'PASSWORD' => $user['password']
                );
                $loginUser {'result_found'} = $AMOUNT_QUERY_RESULTS + 1;
                $loginUser {'LOGIN'} = 'USER FOUND';

                $cookie_login_value = $user['id'];
                $AMOUNT_QUERY_RESULTS++;
            }
        }

        switch ($APP_ID_DETECTED)
        {
            case config::APP_WEB_BROWSER:
                $cookie = new cookies();
                $cookie->createCookie(config::APP_WEB_BROWSER, cookies::COOKIE_NAME_LOGIN_USER, $cookie_login_value, encrypt::ENCRYPT_NO);
                $cookie->createCookie(config::APP_WEB_BROWSER, cookies::COOKIE_NAME_GENERAL, cookies::COOKIE_PASS_GENERAL, encrypt::ENCRYPT_YES);
            break;
        }

        $jsonPretty = new JsonPretty;
        echo $jsonPretty->prettify($loginUser);
        exit();
    }
}

?>