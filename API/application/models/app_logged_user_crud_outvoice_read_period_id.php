<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_outvoice_read_period_id
{  
    public function readOutvoicesForPeriodId()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadOutvoicesForPeriodId';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        if ( isset($_GET['periodId']) && $_GET['periodId'] != '' )
        {
            $periodId = $_GET['periodId'];
        }else
            isRequired( 'Periodo' );

        $PARAMETERS = array( $USER_ID, $periodId );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_outvoice_read_period_id', 'readOutvoicesForPeriodIdJson', $PARAMETERS );
    }

    function readOutvoicesForPeriodIdJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            $amount_total = 0;
            while ( $readOutvoicesForPeriodIdJson = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $amount_total = $amount_total + $readOutvoicesForPeriodIdJson['amount'];
                $contentJson{'total_amount'} = $amount_total;
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'READ_OUTVOICES_PERIODID_ID' => $readOutvoicesForPeriodIdJson['id'],
                    'READ_OUTVOICES_PERIODID_DATA' => array(
                        'READ_OUTVOICES_PERIODID_DATE' => $readOutvoicesForPeriodIdJson['date'],
                        'READ_OUTVOICES_PERIODID_AMOUNT' => $readOutvoicesForPeriodIdJson['amount'],
                        'READ_OUTVOICES_PERIODID_PERIOD_ID' => $readOutvoicesForPeriodIdJson['id_period'],
                        'READ_OUTVOICES_PERIODID_USER_ID' => $readOutvoicesForPeriodIdJson['id_user']
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }
}

?>