<?php

include_once(dirname(__FILE__).'./../config/config.php');

class app_login
{   
    public function loginUser()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'sqlQueryUserLogin';

        if ( isset($_GET['cuit']) && $_GET['cuit'] != '' )
        {
            $cuit = $_GET['cuit'];
        }else
            isRequired( 'Cuit' );

        if ( isset($_GET['password']) && $_GET['password'] != '' )
        {
            $password = $_GET['password'];
            $USER_PASS = encrypt($password, credentials::USER_PATRON_PASS);
        }else
            isRequired( 'password' );

        $PARAMETERS = array( $cuit, $USER_PASS );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_login', 'appLoginJson', $PARAMETERS );
    }

    public function appLoginJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $APP_ID_DETECTED;

        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        //hardware_identification_by_call_type( config::IDENTIFICATION_HARDWARE_LOGIN );

        if (isset($QUERY_DATA_BASE))
        {
            while($user = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $loginUser {'success'} = 1;
                $loginUser{'data'}{'content'}
                [$AMOUNT_QUERY_RESULTS] = array(
                    'ID_USER'  => $user['id'],
                    'CUIT'=> $user['cuit'],
                    'NAME_LASTNAME'=> $user['nameLastName'],
                    'PASSWORD' => $user['password']
                );
                $loginUser {'result_found'} = $AMOUNT_QUERY_RESULTS + 1;
                $loginUser {'LOGIN'} = 'USER FOUND';

                $cookie_login_value = $user['id'];
                $AMOUNT_QUERY_RESULTS++;
            }
        }

        switch ($APP_ID_DETECTED)
        {
            case config::APP_WEB_BROWSER:
                $cookie = new cookies();
                $cookie->createCookie(config::APP_WEB_BROWSER, cookies::COOKIE_NAME_LOGIN_USER, $cookie_login_value, encrypt::ENCRYPT_NO);
                $cookie->createCookie(config::APP_WEB_BROWSER, cookies::COOKIE_NAME_GENERAL, cookies::COOKIE_PASS_GENERAL, encrypt::ENCRYPT_YES);
            break;
        }

        $jsonPretty = new JsonPretty;
        echo $jsonPretty->prettify($loginUser);
        exit();
    }
}

?>