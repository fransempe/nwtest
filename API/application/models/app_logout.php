<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logout
{
    public function logout()
    {
        global $APP_ID_DETECTED, $NAME_APP_DETECTED;

        //$autentication = new autentication();
        //$autentication->checkSession( $APP_ID_DETECTED, $NAME_APP_DETECTED );

        switch ( $APP_ID_DETECTED )
        {
            case config::APP_WEB_BROWSER:
                $cookie = new cookies();
                $cookie->clearCookie(config::APP_WEB_BROWSER, cookies::COOKIE_NAME_LOGIN_USER);
                $cookie->clearCookie(config::APP_WEB_BROWSER, cookies::COOKIE_NAME_GENERAL);
            break;
        }

        $message = "User Logout";
        $jsonPretty = new JsonPretty;
        echo $jsonPretty->prettify (array( 'success' => 1, 'message' => $message ));
        exit();
    }
}
?>