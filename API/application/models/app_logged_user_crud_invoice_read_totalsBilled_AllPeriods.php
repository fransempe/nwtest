<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_invoice_read_totalsBilled_AllPeriods
{  
    public function readTotalsBilledInVoiceAllPeriods()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadInvoicesTotalsBilledForAllPeriods';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        $PARAMETERS = array( $USER_ID );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_invoice_read_totalsBilled_AllPeriods', 'readTotalsBilledAllPeriodsJson', $PARAMETERS );
    }

    function readTotalsBilledAllPeriodsJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            while ( $readTotalsBilledAllPeriodsJson = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {

                $BILLED_TOTAL = $readTotalsBilledAllPeriodsJson['total'];
                $BIMONTLY_AVERAGE = $readTotalsBilledAllPeriodsJson['amountTotalCategory'] / 12;
                $AMOUNT_EXCEEDED = "0";

                if( $BILLED_TOTAL > $BIMONTLY_AVERAGE)
                {
                    $AMOUNT_EXCEEDED = $BILLED_TOTAL - $BIMONTLY_AVERAGE;
                }

                $BIMONTLY_AVERAGE = number_format($BIMONTLY_AVERAGE, 2, '.', '');
                $AMOUNT_EXCEEDED = number_format($AMOUNT_EXCEEDED, 2, '.', '');

                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'READ_INVOICES_BILLEDALLPERIODS_PERIOD_ID' => $readTotalsBilledAllPeriodsJson['period_id'],
                    'READ_INVOICES_BILLEDALLPERIODS_DATA' => array(
                        'READ_INVOICES_BILLEDALLPERIODS_PERIOD_TYPE' => $readTotalsBilledAllPeriodsJson['period_type'],
                        'READ_INVOICES_BILLEDALLPERIODS_AMOUNT_TOTAL' => $readTotalsBilledAllPeriodsJson['total'],
                        'READ_INVOICES_BILLEDALLPERIODS_BIMONTHLY_AVERAGE' => $BIMONTLY_AVERAGE,
                        'READ_INVOICES_BILLEDALLPERIODS_AMOUNT_EXCEEDED' => $AMOUNT_EXCEEDED,
                        'READ_INVOICES_BILLEDALLPERIODS_USER_ID' => $readTotalsBilledAllPeriodsJson['id_user']
                    ),
                    'NUMBER_RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    } 
}
?>