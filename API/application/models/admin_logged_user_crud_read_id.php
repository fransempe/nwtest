<?php
include_once(dirname(__FILE__).'./../config/config.php');

class admin_logged_user_crud_read_id
{  
    public function readUserForId()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadUsersForId';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'ID de usuerio' );

        $PARAMETERS = array( $USER_ID );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'admin_logged_user_crud_read_id', 'readUserForIdJson', $PARAMETERS );
    }

    function readUserForIdJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            while ( $listUser = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'ID_USER' => $listUser['id'],
                    'USER_DATA' => array(
                        'EMAIL' => $listUser['email'],
                        'NAME_LAST_NAME' => $listUser['nameLastName'],
                        'CUIT' => $listUser['cuit'],
                        'HIERARCHY_CLIENT' => $listUser['hierarchy_client'],
                        'CATEGORY_TYPE' => $listUser['category_type'],
                        'CATEGORY_ID' => $listUser['category_id'],
                        'AMOUNT' => $listUser['amount']
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }
}

?>