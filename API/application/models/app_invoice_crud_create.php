<?php

include_once(dirname(__FILE__).'./../config/config.php');

class app_invoice_crud_create
{   
    public function create_invoice()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'create_invoice';

        if ( isset($_GET['userId']) )
        {
            $USER_ID = $_GET['userId']; 
        }else
            isRequired( 'Id de usuario' );

        if ( isset($_GET['periodId']) )
        {
            $periodId = $_GET['periodId']; 
        }else
            isRequired( 'Periodo' );

        if ( isset($_GET['date']) )
        {
            $date = $_GET['date'];
        }else
            isRequired( 'invoicePointSale' );

        if ( isset($_GET['invoicePointSale']) )
        {
            $invoicePointSale = $_GET['invoicePointSale'];
        }else
            isRequired( 'Punto de venta' );

        if ( isset($_GET['amount']) )
        {
            $amount = $_GET['amount'];
        }else
            isRequired( 'Monto total' );

        if ( isset($_GET['numInvoice']) )
        {
            $numInvoice = $_GET['numInvoice']; 
        }else
            isRequired( 'número de factura.' );

        if ( $USER_ID == '' || $date == '' || $amount == '' || $invoicePointSale == '' || $numInvoice == '' )
        {
            missingData( 'incorrect data,' );
        }

        $PARAMETERS = array( $USER_ID, $periodId, $date, $invoicePointSale, $amount, $numInvoice );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_invoice_crud_create', 'appAddInvoiceJson', $PARAMETERS );
    }

    public function appAddInvoiceJson( $QUERY_DATA_BASE, $DETAIL_COMPANY, $PARAMETERS )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Invoice created successfully',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
    }
}

?>