<?php
include_once(dirname(__FILE__).'./../config/config.php');
class company
{

    public function getAllCompany()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryCompanySearch';
        if ( isset( $_GET['company_id'] ) )
        {
            $company_id = $_GET['company_id'];
            $PARAMETERS = array( $company_id );
        }
        else
        {
            $PARAMETERS = array( '' );
        }

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_company', 'getAllCompanyJson', $PARAMETERS );
    }

    function getAllCompanyJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            while ( $company = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'ID_COMPANY' => $company['empresa_id'],
                    'COMPANY_DATA' => array(
                        'RAZON_SOCIAL' => setCompanyDetail($company['empresa_id']),
                        'CUIT' => $company['cuit']
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }
}
?>