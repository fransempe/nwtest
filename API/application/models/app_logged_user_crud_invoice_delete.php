<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_invoice_delete
{  
    public function deleteInvoice()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryDeleteInvoices';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        if ( isset($_GET['invoiceId']) && $_GET['invoiceId'] != '' )
        {
            $invoiceId = $_GET['invoiceId'];
        }else
            isRequired( 'Id de factura de venta' );

        $PARAMETERS = array( $USER_ID, $invoiceId );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_invoice_delete', 'deleteInvoiceJson', $PARAMETERS );
    }

    public function deleteInvoiceJson( $QUERY_DATA_BASE, $DETAIL_COMPANY, $PARAMETERS )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Factura de venta borrada correctamente.',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
    }
}

?>