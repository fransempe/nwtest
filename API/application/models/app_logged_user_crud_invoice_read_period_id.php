<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_invoice_read_period_id
{  
    public function readInvoicesForPeriodId()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadInvoicesForPeriodId';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        if ( isset($_GET['periodId']) && $_GET['periodId'] != '' )
        {
            $periodId = $_GET['periodId'];
        }else
            isRequired( 'Periodo' );

        $PARAMETERS = array( $USER_ID, $periodId );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_invoice_read_period_id', 'readInvoicesForPeriodIdJson', $PARAMETERS );
    }

    function readInvoicesForPeriodIdJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            $amount_total = 0;
            while ( $readInvoicesForPeriodIdJson = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $amount_total = $amount_total + $readInvoicesForPeriodIdJson['amount'];
                $contentJson{'total_amount'} = $amount_total;
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'READ_INVOICES_PERIODID_ID' => $readInvoicesForPeriodIdJson['id'],
                    'READ_INVOICES_PERIODID_DATA' => array(
                         'READ_INVOICES_PERIODID_DATE' => $readInvoicesForPeriodIdJson['date'],
                        'READ_INVOICES_PERIODID_AMOUNT' => $readInvoicesForPeriodIdJson['amount'],
                        'READ_INVOICES_PERIODID_POINT_SALE' => $readInvoicesForPeriodIdJson['pointSale'],
                        'READ_INVOICES_PERIODID_NUM_INVOICE' => $readInvoicesForPeriodIdJson['num_invoice'],
                        'READ_INVOICES_PERIODID_PERIOD_ID' => $readInvoicesForPeriodIdJson['id_period'],
                        'READ_INVOICES_PERIODID_USER_ID' => $readInvoicesForPeriodIdJson['id_user']
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }
}

?>