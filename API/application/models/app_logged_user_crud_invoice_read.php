<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_invoice_read
{  
    public function readInvoices()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadInvoices';

        $PARAMETERS = array( '' );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_invoice_read', 'readInvoicesJson', $PARAMETERS );
    }

    function readInvoicesJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            while ( $readInvoicesJson = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'READ_INVOICES_ID' => $readInvoicesJson['id'],
                    'READ_INVOICES_DATA' => array(
                        'READ_INVOICES_DATE' => $readInvoicesJson['date'],
                        'READ_INVOICES_AMOUNT' => $readInvoicesJson['amount'],
                        'READ_INVOICES_NUM_INVOICE' => $readInvoicesJson['num_invoice'],
                        'READ_INVOICES_PERIOD_ID' => $readInvoicesJson['id_period'],
                        'READ_INVOICES_USER_ID' => $readInvoicesJson['id_user']
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }
}

?>