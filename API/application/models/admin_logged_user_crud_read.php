<?php
include_once(dirname(__FILE__).'./../config/config.php');

class admin_logged_user_crud_read
{  
    public function readUsers()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadUsers';

        $PARAMETERS = array( '' );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'admin_logged_user_list', 'listUsersJson', $PARAMETERS );
    }

    function readUsersJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            while ( $listUser = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'ID_USER' => $listUser['id'],
                    'USER_DATA' => array(
                        'EMAIL' => $listUser['email'],
                        'NAME_LAST_NAME' => $listUser['nameLastName'],
                        'CUIT' => $listUser['cuit'],
                        'HIERARCHY_CLIENT' => $listUser['hierarchy_client'],
                        'CATEGORY_TYPE' => $listUser['category_type'],
                        'AMOUNT' => $listUser['amount']
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }
}

?>