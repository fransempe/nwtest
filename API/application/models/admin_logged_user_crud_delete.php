<?php
include_once(dirname(__FILE__).'./../config/config.php');

class admin_logged_user_crud_delete
{  
    public function deleteUsers()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryDeleteUsers';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $userId = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        $PARAMETERS = array( $userId );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'admin_logged_user_crud_delete', 'deleteUsersJson', $PARAMETERS );
    }

    public function deleteUsersJson( $QUERY_DATA_BASE, $DETAIL_COMPANY, $PARAMETERS )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'message' => 'Usuario borrado correctamente.',
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
    }
}

?>