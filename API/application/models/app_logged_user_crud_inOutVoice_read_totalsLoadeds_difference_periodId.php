<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_inOutVoice_read_totalsLoadeds_difference_periodId
{  
    public function readTotalsLoadedsDifferencePeriodId()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $FIRST_QUERY_TABLE_INVOICES = 'queryReadInvoicesTotalsBilledForPeriodId';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        if ( isset($_GET['periodId']) && $_GET['periodId'] != '' )
        {
            $periodId = $_GET['periodId'];
        }else
            isRequired( 'Id de periodo' );

        $PARAMETERS = array( $USER_ID, $periodId );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $FIRST_QUERY_TABLE_INVOICES, 'app_logged_user_crud_inOutVoice_read_totalsLoadeds_difference_periodId', 'readTotalsLoadedsDifferencePeriodIdJson', $PARAMETERS );
    }        

    function readTotalsLoadedsDifferencePeriodIdJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {

        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        $USER_ID = "";
        $PERIOD_ID = "";
        $TOTAL_INVOICES_BILLED_PERIOD = "";

        if (isset($QUERY_DATA_BASE))
        {
            while ( $readTotalsLoadedsDifferencePeriodIdFirstInvoicesJson = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $TOTAL_INVOICES_BILLED_PERIOD = $readTotalsLoadedsDifferencePeriodIdFirstInvoicesJson['total'];

                $USER_ID = $readTotalsLoadedsDifferencePeriodIdFirstInvoicesJson['id_user'];

                $PERIOD_ID = $readTotalsLoadedsDifferencePeriodIdFirstInvoicesJson['period_id'];
            }
            $PARAMETERS = array( $USER_ID, $PERIOD_ID );
            $SECOND_QUERY_TABLE_OUTVOICES = queryReadOutvoicesTotalsBilledForPeriodId( $PARAMETERS );

            $TOTAL_OUTVOICES_BILLED_PERIOD = "";
            $PERIOD_TYPE = "";

            while ( $readTotalsLoadedsDifferencePeriodIdJson = mysql_fetch_array( $SECOND_QUERY_TABLE_OUTVOICES ) )
            {
                $TOTAL_OUTVOICES_BILLED_PERIOD = $readTotalsLoadedsDifferencePeriodIdJson['total'];
                $PERIOD_TYPE = 
                $readTotalsLoadedsDifferencePeriodIdJson['period_type'];
            }

            $DIFFERENCE_READ_INOUTVOICES = $TOTAL_INVOICES_BILLED_PERIOD - $TOTAL_OUTVOICES_BILLED_PERIOD;

            $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'READ_INOUTVOICES_BILLEDFORPERIODID_PERIOD_ID' => control_null_result( $PERIOD_ID ),
                    'READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_DATA' => array(
                        'READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_PERIOD_TYPE' => control_null_result( $PERIOD_TYPE ),
                        'READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_AMOUNT_TOTAL_INVOICE' => control_null_result( $TOTAL_INVOICES_BILLED_PERIOD ),
                        'READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_AMOUNT_TOTAL_OUTVOICE' => control_null_result( $TOTAL_OUTVOICES_BILLED_PERIOD ),
                        'READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_AMOUNT_TOTAL_DIFFERENCE' => control_null_result( $DIFFERENCE_READ_INOUTVOICES ),
                        'READ_INOUTVOICES_TOTALSLOADEDS_DIFFERENCE_PERIODID_USER_ID' => control_null_result( $USER_ID )
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
        }
    }          
}
?>