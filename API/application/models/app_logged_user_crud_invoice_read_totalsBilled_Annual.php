<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_invoice_read_totalsBilled_Annual
{  
    public function readTotalsBilledInVoiceAnnual()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadInvoicesTotalsBilledForAnnual';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        $PARAMETERS = array( $USER_ID );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_invoice_read_totalsBilled_Annual', 'readTotalsBilledAnnualJson', $PARAMETERS );
    }

    function readTotalsBilledAnnualJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            $ACUM_TOTAL_ANNUAL = 0;
            $ACUM_BIMONTLY_AVERAGE = 0;
            $ACUM_AMOUNT_EXCEEDED = 0;
            $PARAMETER_BIANNUAL_RECATEGORIZATION = 0;
            $LIMIT_BY_CATEGORY = 0;

            while ( $readTotalsBilledAllPeriodsJson = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {

                $LIMIT_BY_CATEGORY = $readTotalsBilledAllPeriodsJson['amountTotalCategory'];
                $BILLED_TOTAL = $readTotalsBilledAllPeriodsJson['total'];
                $BIMONTLY_AVERAGE = $readTotalsBilledAllPeriodsJson['amountTotalCategory'] / 12;

                $PARAMETER_BIANNUAL_RECATEGORIZATION = $readTotalsBilledAllPeriodsJson['amountTotalCategory'] / 2;

                $PARAMETER_BIANNUAL_RECATEGORIZATION = number_format($PARAMETER_BIANNUAL_RECATEGORIZATION, 2, '.', '');

                $AMOUNT_EXCEEDED = "0";

                $ACUM_TOTAL_ANNUAL = $ACUM_TOTAL_ANNUAL + $BILLED_TOTAL;

                if( $BILLED_TOTAL > $BIMONTLY_AVERAGE)
                {
                    $AMOUNT_EXCEEDED = $BILLED_TOTAL - $BIMONTLY_AVERAGE;
                }
                $ACUM_BIMONTLY_AVERAGE = $ACUM_BIMONTLY_AVERAGE + $BIMONTLY_AVERAGE;

                $ACUM_AMOUNT_EXCEEDED = $ACUM_AMOUNT_EXCEEDED + $AMOUNT_EXCEEDED;

                $ACUM_BIMONTLY_AVERAGE = number_format($ACUM_BIMONTLY_AVERAGE, 2, '.', '');

            }

            $ACUM_ANNUAL_AMOUNT_EXCEEDED = $ACUM_TOTAL_ANNUAL - $LIMIT_BY_CATEGORY;

            if( $ACUM_ANNUAL_AMOUNT_EXCEEDED < 0 )
            {
                $ACUM_ANNUAL_AMOUNT_EXCEEDED = '00.00';
            }

            $ACUM_ANNUAL_AMOUNT_EXCEEDED = number_format($ACUM_ANNUAL_AMOUNT_EXCEEDED, 2, '.', '');
            $ACUM_TOTAL_ANNUAL = number_format($ACUM_TOTAL_ANNUAL, 2, '.', '');

            $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'READ_INVOICES_BILLEDANNUALPERIODS_DATA' => array(
                        'READ_INVOICES_BILLEDANNUALPERIODS_TOTAL_ANNUAL' => $ACUM_TOTAL_ANNUAL,
                        'READ_INVOICES_BILLEDANNUALPERIODS_LIMIT_BY_CATEGORY' => $LIMIT_BY_CATEGORY,
                        'READ_INVOICES_BILLEDANNUALPERIODS_TOTAL_SURPASSED' => $ACUM_ANNUAL_AMOUNT_EXCEEDED
                    ),
                    'NUMBER_RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
        }
    } 
}
?>