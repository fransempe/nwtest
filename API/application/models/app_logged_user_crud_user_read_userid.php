<?php
include_once(dirname(__FILE__).'./../config/config.php');
class app_logged_user_crud_user_read_userid
{
    public function readUserId()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadUserId';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $userId = $_GET['userId'];
        }else
            isRequired( 'userId' );

        $PARAMETERS = array( $userId );


        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_user_read_userid', 'readUserIdJson', $PARAMETERS );
    }

    function readUserIdJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        while ( $user = mysql_fetch_array( $QUERY_DATA_BASE ) )
        {
            $contentJson{'data'}{'content'}
            {$AMOUNT_QUERY_RESULTS} = array(
                'ID_USER' => $user['id'],
                'USER_DATA' => array(
                    'EMAIL' => $user['email'],
                    'NAME_LAST_NAME' => $user['nameLastName'],
                    'CUIT' => $user['cuit'],
                    'HIERARCHY_CLIENT_ID' => $user['hierarchy_client_id'],
                    'CATEGORY_ID' => $user['category_id'],
                    'CATEGORY_TYPE' => $user['category_type'],
                    'AMOUNT' => $user['amount'],
                    'HIERARCHY_CLIENT' => $user['hierarchy_client']

                ),
                'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                'COMPANY' => $DETAIL_COMPANY
            );
            $AMOUNT_QUERY_RESULTS++;
        }
    }
}
?>