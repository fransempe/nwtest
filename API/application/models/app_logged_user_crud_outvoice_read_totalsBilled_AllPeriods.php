<?php
include_once(dirname(__FILE__).'./../config/config.php');

class app_logged_user_crud_outvoice_read_totalsBilled_AllPeriods
{  
    public function readTotalsBilledOutVoiceAllPeriods()
    {
        $COMPANY = company::COMPANY_DETAIL;
        $QUERY_TABLE = 'queryReadOutvoicesTotalsBilledForAllPeriods';

        if ( isset($_GET['userId']) && $_GET['userId'] != '' )
        {
            $USER_ID = $_GET['userId'];
        }else
            isRequired( 'Id de usuario' );

        $PARAMETERS = array( $USER_ID );

        $connect = new connect();
        $connect -> connectDataBase ( $COMPANY,  $QUERY_TABLE, 'app_logged_user_crud_outvoice_read_totalsBilled_AllPeriods', 'readTotalsBilledOutVoiceAllPeriodsJson', $PARAMETERS );
    }

    function readTotalsBilledOutVoiceAllPeriodsJson( $QUERY_DATA_BASE, $DETAIL_COMPANY )
    {
        global $AMOUNT_QUERY_RESULTS;
        global $contentJson;

        if (isset($QUERY_DATA_BASE))
        {
            while ( $readTotalsBilledOutVoiceAllPeriodsJson = mysql_fetch_array( $QUERY_DATA_BASE ) )
            {
                $contentJson{'data'}{'content'}
                {$AMOUNT_QUERY_RESULTS} = array(
                    'READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_ID' => $readTotalsBilledOutVoiceAllPeriodsJson['period_id'],
                    'READ_OUTVOICES_BILLEDALLPERIODS_DATA' => array(
                        'READ_OUTVOICES_BILLEDALLPERIODS_PERIOD_TYPE' => $readTotalsBilledOutVoiceAllPeriodsJson['period_type'],
                        'READ_OUTVOICES_BILLEDALLPERIODS_AMOUNT_TOTAL' => $readTotalsBilledOutVoiceAllPeriodsJson['total'],
                        'READ_OUTVOICES_BILLEDALLPERIODS_USER_ID' => $readTotalsBilledOutVoiceAllPeriodsJson['id_user']
                    ),
                    'NUMBER RESULT' => $AMOUNT_QUERY_RESULTS + 1,
                    'COMPANY' => $DETAIL_COMPANY
                );
                $AMOUNT_QUERY_RESULTS++;
            }
        }
    }           
}
?>