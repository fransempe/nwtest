<?php
include_once(dirname(__FILE__).'./../../config.php');

class cookies
{
    const COOKIE_NAME_LOGIN_USER = "L_USER";
    const COOKIE_NAME_GENERAL = "NW7772019";
    const COOKIE_PASS_GENERAL = "NWGOT2019";
    const COOKIE_EXIST = 1;
    const COOKIE_NOT_EXIST = 0;
    const COOKIE_PATH = "/NWTest/";

    Function createCookie( $APP_ID, $COOKIE_NAME, $COOKIE_PASS, $ENCRYPT )
    {
        $COOKIE_TIME = (3600 * 24 * 30); // 30 days (3600 * 24 * 30)
        $COOKIE_PATRON_PASS ="kbnrtvfe607479eBNK";
        switch ($ENCRYPT)
        {
            case encrypt::ENCRYPT_YES:
                $COOKIE_KEY = encrypt($COOKIE_PASS, $COOKIE_PATRON_PASS);
            break;
            case encrypt::ENCRYPT_NO:
                $COOKIE_KEY = $COOKIE_PASS;
            break;
        }
        switch ( $APP_ID )
        {
            case config::APP_WEB_BROWSER:
                setcookie($COOKIE_NAME, $COOKIE_KEY, time() + $COOKIE_TIME, cookies::COOKIE_PATH, config::HOST);
            break;
        }
    }

    Function cookiesExist()
    {
        if (isset($_COOKIE[cookies::COOKIE_NAME_GENERAL]))
        {
            if (isset($_COOKIE[cookies::COOKIE_NAME_LOGIN_USER]))
            {
                return( cookies::COOKIE_EXIST );
            }
            else
            {
                return( cookies::COOKIE_NOT_EXIST );
            }
        }
        else
        {
            return(cookies::COOKIE_NOT_EXIST);
        }
    }

    function clearCookie( $APP_ID, $COOKIE_NAME )
    {
        $CLEAR = "";
        $COOKIE_TIME = (3600 * 24 * 30); // 30 days (3600 * 24 * 30)

        switch ( $APP_ID )
        {
            case config::APP_WEB_BROWSER:
                setcookie($COOKIE_NAME, $CLEAR, time() - $COOKIE_TIME, cookies::COOKIE_PATH, config::HOST);
                break;
        }
    }
}
?>