<?php
include_once(dirname(__FILE__).'./../responses/errors.php');
include_once(dirname(__FILE__).'./../../models/app_user_identification.php');
include_once(dirname(__FILE__).'./../../models/identification_hardware.php');

class autentication
{

    function checkHardware ( $HARDWARE_OS_DEVICENAME, $DEVICE_DETAIL, $MAC_ADDRESS_NET_ADAPTER, $NET_IP_ADDRESS, $NET_PORT, $APP_ID, $NAME_APP )
    {
        $identificationHardware = new app_identification_hardware();
        $identificationHardware->identification_hardware( $HARDWARE_OS_DEVICENAME, $DEVICE_DETAIL, $MAC_ADDRESS_NET_ADAPTER, $NET_IP_ADDRESS, $NET_PORT, $APP_ID, $NAME_APP );
    }

    function checkSession( $APP_ID )
    {
        switch ( $APP_ID )
        {
            case config::APP_WEB_BROWSER:
                $cookiesExist = new cookies();
                switch ($cookiesExist->cookiesExist())
                {
                    case cookies::COOKIE_EXIST:
                        $identificationUser = new app_identification_user();
                        $identificationUser->identification_user($_COOKIE[cookies::COOKIE_NAME_LOGIN_USER]);
                    break;
                    case cookies::COOKIE_NOT_EXIST:
                         userNotFound( config::DEVELOPER_NAME );
                    break;
                }
            break;
            case config::APP_IOS:
                userNotFound( config::DEVELOPER_NAME );
            break;
            case config::APP_ANDROID:
                userNotFound( config::DEVELOPER_NAME );
            break;
            case config::APP_WINDOWS_PHONE:
                userNotFound( config::DEVELOPER_NAME );
            break;
         }
    }
}

?>