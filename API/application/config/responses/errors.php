<?php
include_once(dirname(__FILE__).'./../config.php');

function loginSetDataTableError( $table )
{
    if(!$table)
    {
        sentLogEmail ();
        setError( 'DATA TABLE NOT FOUND', -1 );
    }
    if( @mysql_num_rows( $table ) == 0 )
    {
        setError( 'Cuit o Clave incorrectos', 0 );
    }
}

function setDataTableError( $table )
{
    if(!$table)
    {
        sentLogEmail ();
        setError( 'DATA TABLE NOT FOUND', -1 );
    }
    /*I commented this code for not data loaded in the data base.*/
    //if( mysql_num_rows($table) == 0 )
       // setError( 'NO DATA LOADED INTO THE DATA BASE', 0 );
}

function addDataTableError( $table )
{
    if(!$table)
    {
        sentLogEmail ();
        setError( 'DATA TABLE NOT FOUND', -1 );
    }
}

function editDataTableError( $table )
{
    if(!$table)
    {
        sentLogEmail ();
        setError( 'DATA TABLE NOT FOUND', -1 );
    }
}

function isRequired( $data )
{
    setError($data.' es obligatorio', 0);
}

function userNotFound( $data )
{
    setError($data.' User not found!', 0);
}

function missingData( $data )
{
    setError($data.' Missing data', 0);
}

function validateMail()
{
    setError('La casilla de correo no es valida.', 0);
}

function passwordDoNotMatch()
{
    setError('Passwords do not match', 0);
}

function setError( $message, $success )
{
    $jsonPretty = new JsonPretty;
    echo $jsonPretty->prettify (array( 'success' => $success, 'data' => '', 'message' => $message ));
    exit;
}

?>