<?php
include_once(dirname(__FILE__).'./../config.php');

class apps
{
    function detectApp()
    {
        /*************APP BROWSER*************/
            if (isset($_SERVER['HTTP_USER_AGENT']))
            {
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
                function getBrowser($user_agent)
                {
                    if(strpos($user_agent, 'MSIE') !== FALSE)
                        return 'Internet explorer';
                    elseif(strpos($user_agent, 'Edge') !== FALSE) //Microsoft Edge
                        return 'Microsoft Edge';
                    elseif(strpos($user_agent, 'Trident') !== FALSE) //IE 11
                        return 'Internet explorer';
                    elseif(strpos($user_agent, 'Opera Mini') !== FALSE)
                        return "Opera Mini";
                    elseif(strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE)
                        return "Opera";
                    elseif(strpos($user_agent, 'Firefox') !== FALSE)
                        return 'Mozilla Firefox';
                    elseif(strpos($user_agent, 'Chrome') !== FALSE)
                        return 'Google Chrome';
                    elseif(strpos($user_agent, 'Safari') !== FALSE)
                        return "Safari";
                    else
                        die("browser not found");
                }
                $NAME_WEB_BROWSER = getBrowser($user_agent);
                return array ( config::APP_WEB_BROWSER, $NAME_WEB_BROWSER );
            }
            else
            {
                die("browser not found");
            }
    }

    public function app_type($APP_ID)
    {

        switch ($APP_ID)
        {
            case config::APP_WEB_BROWSER:
                $APP = "APP_WEB_BROWSER";
                break;
            case config::APP_IOS:
                $APP = "APP_iOS";
                break;
            case config::APP_ANDROID:
                $APP = "APP_ANDROID";
                break;
            case config::APP_WINDOWS_PHONE:
                $APP = "APP_WINDOWS_PHONE";
                break;
            case config::APP_DESCKTOP:
                $APP = "APP_DESCKTOP";
                break;
        }
        return $APP;
    }
}
?>