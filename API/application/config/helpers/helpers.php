<?php
include_once(dirname(__FILE__).'./../config.php');

/******Json vars**********************/
global $AMOUNT_QUERY_RESULTS;
$AMOUNT_QUERY_RESULTS = 0;
global $contentJson;
$contentJson = array();

function remove_tildes( $string ) {
    $not_allowed= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
    $alloweed= array    ("a","e","i","o","u","A","E","I","O","U","n","N","A","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
    $text = str_replace($not_allowed, $alloweed ,$string);
    return $text;
}

function setCompanyDetail( $COMPANY_DETAIL )
{
	switch ( $COMPANY_DETAIL ) 
	{
        case company::UTE:
            $COMPANY_DETAIL = company::UTE_DETAIL;
        break;
		case company::DOCE_OCTUBRE:
			$COMPANY_DETAIL = company::DOCE_OCTUBRE_DETAIL;
		break;
		case company::EL_LIBERTADOR:
			$COMPANY_DETAIL = company::EL_LIBERTADOR_DETAIL;
		break;
		case company::VEINTICINCO_DE_MAYO:
			$COMPANY_DETAIL = company::VEINTICINCO_DE_MAYO_DETAIL;
		break;
		case company::PERALTA_RAMOS:
			$COMPANY_DETAIL = company::PERALTA_RAMOS_DETAIL;
		break;
		case company::COSTA_AZUL:
			$COMPANY_DETAIL = company::COSTA_AZUL_DETAIL;
		break;
	}
	return ( $COMPANY_DETAIL );
}

function email_validate_large( $email )
{
   $mail_correcto = 0;
   //compruebo unas cosas primeras
   if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@"))
   {
      if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) 
      {
         //miro si tiene caracter .
         if (substr_count($email,".")>= 1)
         {
            //obtengo la terminacion del dominio
            $term_dom = substr(strrchr ($email, '.'),1);
            //compruebo que la terminación del dominio sea correcta
            if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) )
            {
               //compruebo que lo de antes del dominio sea correcto
               $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
               $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
               if ($caracter_ult != "@" && $caracter_ult != ".")
               {
                  $mail_correcto = 1;
               }
            }
         }
      }
   }

   if ( $mail_correcto )
      return 1;
   else
      return 0;
}

function email_validate_short( $email )
{
    return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? "1" : "0";
}

function control_null_result( $result )
{
    if ( $result == null )
    {
      $result = "0";
    }
    return ( $result );
}

?>