<?php
//Paths for routes.
define('APPPATH', 'application/');

//Include conection file and configs files
include_once('conection/company.php');
include_once('conection/data_base.php');
include_once('conection/connect.php');
include_once('responses/errors.php');
include_once('responses/correct.php');
include_once('json/JsonPretty.php');
include_once('helpers/helpers.php');

//Hardwares Files
include_once('hardware/apps.php');
include_once('hardware/devices.php');
include_once('hardware/hardware.php');
//include_once('hardware/netWorking.php');
include_once('hardware/macAddress.php');
include_once('hardware/operativeSystems.php');

//Security files
include_once('security/access.php');
include_once('security/credentials/cookies.php');
include_once('security/credentials/credentials.php');
include_once('security/credentials/encrypt.php');
include_once('security/responseValidatorAdmin.php');
include_once('security/responseValidatorUser.php');
include_once('security/autentication.php');
include_once('security/permissions.php');

//Sql files
include_once('sql/sql.php');
include_once('sql/login.php');
include_once('sql/hardware.php');
include_once('sql/identification.php');
include_once('sql/controls.php');
include_once('sql/lists.php');
include_once('sql/creates.php');
include_once('sql/reads.php');
include_once('sql/updates.php');
include_once('sql/deletes.php');
include_once('sql/search.php');
include_once('sql/testing.php');

//Sent emails files
include_once('eMails/sentEmails.php');

global $AMOUNT_QUERY_RESULTS;
$AMOUNT_QUERY_RESULTS = 0;
global $contentJson;
$contentJson = array();

class config
{
    //hardware identification by call type
        const IDENTIFICATION_HARDWARE_LOGIN = 1;
        const IDENTIFICATION_HARDWARE_CALL = 2;

    //APPS
        const APP_WEB_BROWSER = "1";
        const APP_IOS = "2";
        const APP_ANDROID = "3";
        const APP_WINDOWS_PHONE = "4";
        const APP_DESCKTOP = "5";

    //Conection Config
        //const HOST = "localhost";
        //const API_PATH = "/NWTest/API/index.php/";
    //Online
        const HOST = "http://nw.efene.xyz/";
        const API_PATH = "API/index.php/";

    /****Data Base parameters*****/
        const DATA_BASE_USER = "root";

        const DATA_BASE_USER_PASS ="";

    //Developers Names
        const DEVELOPER_NAME = "FN/Software Factory: ";

}

/*******Detect user hardware with api use*********************/
$hardware = new hardware();
$HARDWARE_OS_DEVICENAME = $hardware->detectHardware();

$devices = new devices();
$DEVICE_DETAIL = $devices->deviceDetective();

$macAddress = new macAddress();
$MAC_ADDRESS_NET_ADAPTER = $macAddress->returnMacAddressNetAdapter();

/*$networking = new networking();
$NET_IP_ADDRESS = $networking->getRealIP();
$NET_PORT = $networking->detectPort();*/

$apps = new apps();
list ($APP_ID, $NAME_APP) = $apps->detectApp();

/*$autentication = new autentication();
$autentication->checkHardware( $HARDWARE_OS_DEVICENAME, $DEVICE_DETAIL, $MAC_ADDRESS_NET_ADAPTER, $NET_IP_ADDRESS, $NET_PORT, $APP_ID, $NAME_APP );*/

$APP_ID_DETECTED = $APP_ID;
$NAME_APP_DETECTED = $NAME_APP;


function hardware_identification_by_call_type( $TYPE )
{
    global $HARDWARE_OS_DEVICENAME, $DEVICE_DETAIL, $MAC_ADDRESS_NET_ADAPTER, $NET_IP_ADDRESS, $NET_PORT, $APP_ID_DETECTED, $NAME_APP_DETECTED;
    $apps = new apps();
    $APP_DETECTED_TYPE = $apps->app_type($APP_ID_DETECTED);
    //$queryHardwareSearch = hardwareSearch( $HARDWARE_OS_DEVICENAME, $DEVICE_DETAIL, $MAC_ADDRESS_NET_ADAPTER, $NET_IP_ADDRESS, $NET_PORT, $APP_DETECTED_TYPE, $NAME_APP_DETECTED );
    $AMOUNT_QUERY_RESULTS = 0;
    $hardwareUser = array();
    $hardwareUser {'hardware_success'} = 1;
    $hardwareUser {'SEARCH HARDWARE'} = 'unanswered';
 /*   while( $hardware = mysql_fetch_array( $queryHardwareSearch ) )
    {
        $hardwareUser {'result_found'} = $AMOUNT_QUERY_RESULTS + 1;
        $hardwareUser{'data'}{'Identification'}
        [$AMOUNT_QUERY_RESULTS] = array(
            'MAC ADDRESS NET ADAPTER' => $MAC_ADDRESS_NET_ADAPTER,
            'NET_IP_ADDRESS' => $NET_IP_ADDRESS,
            'NET_PORT' => $NET_PORT,
            'HARDWARE_OS_DEVICENAME'  => $HARDWARE_OS_DEVICENAME,
            'DEVICE'=> $DEVICE_DETAIL,
            'APP' => $APP_DETECTED_TYPE,
            'NAME_APP' => $NAME_APP_DETECTED
        );
        $AMOUNT_QUERY_RESULTS++;
    }*/
    //insertUserHardwareLogin( $HARDWARE_OS_DEVICENAME, $DEVICE_DETAIL, $MAC_ADDRESS_NET_ADAPTER, $NET_IP_ADDRESS, $NET_PORT, $APP_DETECTED_TYPE, $NAME_APP_DETECTED );
    //$jsonPretty = new JsonPretty;
    //echo $jsonPretty->prettify($hardwareUser);
    switch ( $TYPE )
    {
        case config::IDENTIFICATION_HARDWARE_LOGIN:
        break;
        case config::IDENTIFICATION_HARDWARE_CALL:
            //insertUserHardwareLogin( $HARDWARE_OS_DEVICENAME, $DEVICE_DETAIL, $MAC_ADDRESS_NET_ADAPTER, $NET_IP_ADDRESS, $NET_PORT, $APP_DETECTED_TYPE, $NAME_APP_DETECTED );
        break;
    }
}
?>