<?php
//Config
include_once('application/config/config.php');

$uri	= !isset($_SERVER['PATH_INFO']) ? '' : $_SERVER['PATH_INFO'];
$x		= explode('/', $uri);
$class	= ( isset( $x[1] ) && trim( $x[1] ) != '' ) ? $x[1] : 'dummy';
$method = ( isset( $x[2] ) && trim( $x[2] ) != '' ) ? $x[2] : 'index';

    // check dir
    if ( !is_dir(APPPATH.'models/') )
    {
        die( 'Folder not found' );
    }

    // check controller file
    if ( !file_exists(APPPATH.'models/'. $class.'.php') )
    {
        die( 'File not found, enter: ' . 'models/' . $class . '.php' );
    }

    include_once(APPPATH.'models/'. $class.'.php');

    // check controller class
    if ( !class_exists($class))
        die( 'Class not found: ' . $class );

    $obj = new $class();

    // check controller method
    if ( !method_exists ( $obj, $method ) )
        die('Method does not exist: ' . $class . '/' . $method );

    header('Cache-Control: no-cache');

    $obj->$method();
?>